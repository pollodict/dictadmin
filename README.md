# dictadmin

## System information

* JDK version: 17
* framework: Grails 5.3.2
* DB: postgres 14

---

## 相關開發紀錄

[worknotes](https://gitlab.com/pollodict/worknotes) 

## 技術學習資源

[Bootstrap v5.0.2](https://getbootstrap.com/docs/5.0/getting-started/introduction/)