package bs

import groovy.sql.GroovyRowResult

class DictAdminController {

    NavService navService

    /**
     * 登入後系統進入點
     * @return
     */
    def index() {
        List<GroovyRowResult> groupL = navService.getUserUseGroupList()
        List<GroovyRowResult> appL = navService.getUserAppList()
        render view:"/bs/dictAdmin/index", model: [groupL:groupL,appL:appL]
    }
}
