package bs

import grails.converters.JSON

/**
 * 系統程式群組
 */
class Bs110Controller {

    BsAppGroupService bsAppGroupService
    BsAppListGroupService bsAppListGroupService

    /**
     * page: 程式首頁
     * @return
     */
    def index() {
        render view: "/bs/bs110/index"
    }

    /**
     * page: 新增頁面
     */
    def creatPage = {
        BsAppGroup bsAppGroupI = new BsAppGroup()
        render view: "/bs/bs110/creatPage", model: [bsAppGroupI:bsAppGroupI]
    }

    /**
     * page: 編輯頁面
     */
    def editPage = {
        BsAppGroup bsAppGroupI = BsAppGroup.get(params.id as long)
        render view: "/bs/bs110/editPage", model: [bsAppGroupI:bsAppGroupI]
    }

    /**
     * tab: 編輯程式群組資訊
     */
    def editBsAppGroup = {
        BsAppGroup bsAppGroupI = BsAppGroup.get(params.id as long)
        render template: "/bs/bs110/tabs/editBsAppGroup", model: [bsAppGroupI:bsAppGroupI]
    }

    /**
     * tab: 顯示群組程式
     */
    def bsAppListGroupList = {
        BsAppGroup bsAppGroupI = BsAppGroup.get(params.id as long)
        render template: "/bs/bs110/tabs/bsAppListGroupList", model: [bsAppGroupI:bsAppGroupI]
    }

    /**
     * 新增程式
     */
    def creatPageBsAppListGroup = {
        BsAppListGroup bsAppListGroupI = new BsAppListGroup(appGroupNo:params.appGroupNo)
        render template: "/bs/bs110/modalContent/creatModel", model: [
                modelTitle:params.modelTitle,modelId:params.modelId, // model 固定資訊
                bsAppListGroupI:bsAppListGroupI
        ]
    }

    /**
     * 檢視、編輯程式
     */
    def editPageBsAppListGroup = {
        BsAppListGroup bsAppListGroupI = BsAppListGroup.get(params.id as long)
        render template: "/bs/bs110/modalContent/editModel", model: [
                modelTitle:params.modelTitle,modelId:params.modelId, // model 固定資訊
                bsAppListGroupI:bsAppListGroupI
        ]
    }

    /**
     * 查詢
     * @return
     */
    JSON filter(){
        LinkedHashMap result = bsAppGroupService.filter(params)
        render result as JSON
    }

    /**
     * 新增
     * @return
     */
    JSON insert(){
        LinkedHashMap result = bsAppGroupService.insert(params)
        List scriptArrays = []
        if(!result.actionType){
            scriptArrays << [mode: 'execute', script: """showAlert('form-alert','${g.render(template: '/system/base/errorMessage' , model: [beanI:result.bean])}');"""]
        }
        else{
            //更新tab資訊

            scriptArrays << [mode: 'execute', script: """parent.openTab(
                    'BS110',
                    'BS110-${result.bean?.id}',
                    '${result.bean?.appGroupNo?:""}',
                    '${createLink(controller: 'bs110',action: 'editPage',params: [id:result.bean?.id])}'
                );"""]
            scriptArrays << [mode: 'execute', script: """parent.closeTab('BS110-0');"""]
        }
        result.scriptArrays = scriptArrays
        render result as JSON
    }

    /**
     * 更新資料
     * @return
     */
    JSON update(){
        LinkedHashMap result = bsAppGroupService.update(params)
        List scriptArrays = []
        if(!result.actionType){
            scriptArrays << [mode: 'execute', script: """showAlert('form-alert','${g.render(template: '/system/base/errorMessage' , model: [beanI:result.bean])}');"""]
        }
        else{
            //更新tab資訊
            scriptArrays << [mode: 'execute', script: """parent.refreshTab({
                        tabId:'BS110-${result.bean?.id}',
                        src: '${createLink(controller: 'bs110',action: 'editPage',params: [id:result.bean?.id])}'
                    }
                );"""]

        }
        result.scriptArrays = scriptArrays
        render result as JSON
    }

    /**
     * 刪除
     * @return
     */
    JSON delete(){
        LinkedHashMap result = bsAppGroupService.delete(params)
        List scriptArrays = []
        if(!result.actionType){
            scriptArrays << [mode: 'execute', script: """showAlert('form-alert','${g.render(template: '/system/base/errorMessage' , model: [beanI:result.bean])}');"""]
        }else{
            scriptArrays << [mode: 'execute', script: """parent.closeTab('BS110-${result.bean?.id}');"""]
        }

        result.scriptArrays = scriptArrays
        render result as JSON

    }

    /**
     * 程式群組查詢
     * @return
     */
    JSON bsAppListGroupFilter(){
        LinkedHashMap result = bsAppListGroupService.filter(params)
        render result as JSON
    }

    /**
     * 新增程式群組
     * @return
     */
    JSON insertsAppListGroup(){
        LinkedHashMap result = bsAppListGroupService.insert(params)
        List scriptArrays = []
        if(!result.actionType){
            scriptArrays << [mode: 'execute', script: """showAlert('model-alert','${g.render(template: '/system/base/errorMessage' , model: [beanI:result.bean])}');"""]
        }else{

            String modelId = params?.modelId
            String modelTitle = result.bean.appGroupNo
            String url = createLink(controller: "bs110",action: "editPageBsAppListGroup",params: [id:result.bean.id])
            scriptArrays << [mode: 'execute', script: """refreshModel('${modelId}','${modelTitle}','${url}');"""]
            scriptArrays << [mode: 'execute', script: """filterSearchBsAppListGroup();"""]
        }
        result.scriptArrays = scriptArrays
        render result as JSON
    }


    /**
     * 編輯程式群組
     * @return
     */
    JSON updateBsAppListGroup(){
        LinkedHashMap result = bsAppListGroupService.update(params)
        List scriptArrays = []
        if(!result.actionType){
            scriptArrays << [mode: 'execute', script: """showAlert('model-alert','${g.render(template: '/system/base/errorMessage' , model: [beanI:result.bean])}');"""]
        }else{
            String modelId = params?.modelId
            String modelTitle = params?.modelTitle
            String url = createLink(controller: "bs110",action: "editPageBsAppListGroup",params: [id:result.bean.id])
            scriptArrays << [mode: 'execute', script: """refreshModel('${modelId}','${modelTitle}','${url}');"""]
            scriptArrays << [mode: 'execute', script: """filterSearchBsAppListGroup();"""]

        }
        result.scriptArrays = scriptArrays
        render result as JSON
    }

    /**
     * 刪除程式群組
     * @return
     */
    JSON deleteBsAppListGroup(){
        LinkedHashMap result = bsAppListGroupService.delete(params)
        List scriptArrays = []
        if(!result.actionType){
            scriptArrays << [mode: 'execute', script: """showAlert('model-alert','${g.render(template: '/system/base/errorMessage' , model: [beanI:result.bean])}');"""]
        }else{

            String modelId = params?.modelId
            scriptArrays << [mode: 'execute', script: """closeModel('${modelId}');"""]
            scriptArrays << [mode: 'execute', script: """filterSearchBsAppListGroup();"""]
        }
        result.scriptArrays = scriptArrays
        render result as JSON
    }
    
}
