package bs

import grails.converters.JSON

class Bs200Controller {
    
    Bs100Service bs100Service
    Bs101Service bs101Service


    /**
     * page: 程式首頁
     * @return
     */
    def index = {
        render view: "/bs/bs200/index"
    }

    /**
     * page: 新增頁面
     */
    def creatPageBS100 = {
        Bs100 bs100I = new Bs100()
        render view: "/bs/bs200/bs100/creatPage", model: [bs100I:bs100I]
    }

    /**
     * page: 編輯頁面
     */
    def editPageBs100 = {
        Bs100 bs100I = Bs100.get(params.id as long)
        render view: "/bs/bs200/bs100/editPage", model: [bs100I:bs100I]
    }

    /**
     * 查詢
     * @return
     */
    JSON filterBs100(){
        LinkedHashMap result = bs100Service.filter(params)
        render result as JSON
    }

    /**
     * 新增
     * @return
     */
    JSON insertBs100(){
        LinkedHashMap result = bs100Service.insert(params)
        if(!result.actionType){
            result.actionMessage = g.render(template: '/system/base/errorMessage' , model: [beanI:result.bean])
        }
        else{
            //更新tab資訊
            List scriptArrays = []
            scriptArrays << [mode: 'execute', script: """parent.openTab(
                    'BS200',
                    'BS200-${result.bean?.id}',
                    '${result.bean?.ptype?:""}',
                    '${createLink(controller: 'bs200',action: 'editPageBs100',params: [id:result.bean?.id])}'
                );"""]
            scriptArrays << [mode: 'execute', script: """parent.closeTab('BS200-0');"""]
            result.scriptArrays = scriptArrays
        }

        render result as JSON
    }

    /**
     * 更新資料
     * @return
     */
    JSON updateBs100(){
        LinkedHashMap result = bs100Service.update(params)
        if(!result.actionType){
            result.actionMessage = g.render(template: '/system/base/errorMessage' , model: [beanI:result.bean])
        }
        else{
            //更新tab資訊

            List scriptArrays = []
            scriptArrays << [mode: 'execute', script: """parent.refreshTab({
                        tabId:'BS200-${result.bean?.id}',
                        src: '${createLink(controller: 'bs200',action: 'editPageBs100',params: [id:result.bean?.id])}'
                    }
                );"""]
            result.scriptArrays = scriptArrays
        }

        render result as JSON
    }

    /**
     * 刪除
     * @return
     */
    JSON deleteBs100(){
        LinkedHashMap result = bs100Service.delete(params)
        List scriptArrays = []
        if(!result.actionType){
            result.actionMessage = g.render(template: '/system/base/errorMessage' , model: [beanI:result.bean])
        }else{
            scriptArrays << [mode: 'execute', script: """parent.closeTab('BS200-${result.bean?.id}');"""]
            result.scriptArrays = scriptArrays
        }
        render result as JSON

    }

    /**
     * page: 編輯頁面
     */
    def editBs100 () {
        Bs100 bs100I = Bs100.get(params.id as long)
        render template: "/bs/bs200/tabs/editBs100", model: [bs100I:bs100I]
    }

    /**
     * tab: 編輯程式群組資訊
     */
    def bs101List () {
        Bs100 bs100I = Bs100.get(params.id as long)
        render template: "/bs/bs200/tabs/bs101List", model: [bs100I:bs100I]
    }

    /**
     * 新增選項
     */
    def creatBs101 () {
        Bs100 bs100I = Bs100.read(params?.id as long)
        Bs101 bs101I = new Bs101(bs100: bs100I,ptype: bs100I.ptype)
        render template: "/bs/bs200/modalContent/bs101CreatModel", model: [
                modelTitle:params.modelTitle,modelId:params.modelId, // model 固定資訊
                bs101I:bs101I
        ]
    }

    /**
     * 檢視、編輯 選項
     */
    def editBs101 (){
        Bs101 bs101I = Bs101.read(params.id as long)
        render template: "/bs/bs200/modalContent/bs101EditModel", model: [
                modelTitle:params.modelTitle,modelId:params.modelId, // model 固定資訊
                bs101I:bs101I
        ]
    }

    /**
     * 查詢 選項
     * @return
     */
    JSON bs101Filter(){
        LinkedHashMap result = bs101Service.filter(params)
        render result as JSON
    }

    /**
     * 新增 選項
     * @return
     */
    JSON insertBs101(){
        LinkedHashMap result = bs101Service.insert(params)
        List scriptArrays = []
        if(!result.actionType){
            scriptArrays << [mode: 'execute', script: """showAlert('model-alert','${g.render(template: '/system/base/errorMessage' , model: [beanI:result.bean])}');"""]
        }else{

            String modelId = params?.modelId
            String modelTitle = result.bean.pcode
            String url = createLink(controller: "bs200",action: "editBs101",params: [id:result.bean.id])
            scriptArrays << [mode: 'execute', script: """refreshModel('${modelId}','${modelTitle}','${url}');"""]
            scriptArrays << [mode: 'execute', script: """filterSearchBsAppListGroup();"""]
        }
        result.scriptArrays = scriptArrays
        render result as JSON
    }


    /**
     * 編輯 選項
     * @return
     */
    JSON updateBs101(){
        LinkedHashMap result = bs101Service.update(params)
        List scriptArrays = []
        if(!result.actionType){
            scriptArrays << [mode: 'execute', script: """showAlert('model-alert','${g.render(template: '/system/base/errorMessage' , model: [beanI:result.bean])}');"""]
        }else{
            String modelId = params?.modelId
            String modelTitle = params?.modelTitle
            String url = createLink(controller: "bs200",action: "editBs101",params: [id:result.bean.id])
            scriptArrays << [mode: 'execute', script: """refreshModel('${modelId}','${modelTitle}','${url}');"""]
            scriptArrays << [mode: 'execute', script: """filterSearchBsAppListGroup();"""]

        }
        result.scriptArrays = scriptArrays
        render result as JSON
    }

    /**
     * 刪除 選項
     * @return
     */
    JSON deleteBs101(){
        LinkedHashMap result = bs101Service.delete(params)
        List scriptArrays = []
        if(!result.actionType){
            scriptArrays << [mode: 'execute', script: """showAlert('model-alert','${g.render(template: '/system/base/errorMessage' , model: [beanI:result.bean])}');"""]
        }else{

            String modelId = params?.modelId
            scriptArrays << [mode: 'execute', script: """closeModel('${modelId}');"""]
            scriptArrays << [mode: 'execute', script: """filterSearchBsAppListGroup();"""]
        }
        result.scriptArrays = scriptArrays
        render result as JSON
    }

}
