package bs

import grails.converters.JSON
import tw.com.pollochang.security.BsRequestMap
import tw.com.pollochang.security.BsRequestMapService

class Bs100Controller {

    BsRequestMapService bsRequestMapService

    /**
     * page: 程式首頁
     * @return
     */
    def index = {
        render view: "/bs/bs100/index"
    }

    /**
     * page: 新增頁面
     */
    def creatPage = {
        BsRequestMap bsRequestMapI = new BsRequestMap()
        render view: "/bs/bs100/creatPage", model: [bsRequestMapI:bsRequestMapI]
    }

    /**
     * page: 編輯頁面
     */
    def editPage = {
        BsRequestMap bsRequestMapI = BsRequestMap.get(params.id as long)
        render view: "/bs/bs100/editPage", model: [bsRequestMapI:bsRequestMapI]
    }

    /**
     * 查詢
     * @return
     */
    JSON filterBsRequestMap(){
        LinkedHashMap result = bsRequestMapService.filter(params)
        render result as JSON
    }

    /**
     * 新增
     * @return
     */
    JSON insertBsRequestMap(){
        LinkedHashMap result = bsRequestMapService.insert(params)
        if(!result.actionType){
            result.actionMessage = g.render(template: '/system/base/errorMessage' , model: [beanI:result.bean])
        }
        else{
            //更新tab資訊

            List scriptArrays = []
            scriptArrays << [mode: 'execute', script: """parent.openTab(
                    'BS100',
                    'BS100-${result.bean?.id}',
                    '${result.bean?.appNo?:""}',
                    '${createLink(controller: 'bs100',action: 'editPage',params: [id:result.bean?.id])}'
                );"""]
            scriptArrays << [mode: 'execute', script: """parent.closeTab('BS100-0');"""]
            result.scriptArrays = scriptArrays
        }

        render result as JSON
    }

    /**
     * 更新資料
     * @return
     */
    JSON updateBsRequestMap(){
        LinkedHashMap result = bsRequestMapService.update(params)
        if(!result.actionType){
            result.actionMessage = g.render(template: '/system/base/errorMessage' , model: [beanI:result.bean])
        }
        else{
            //更新tab資訊

            List scriptArrays = []
            scriptArrays << [mode: 'execute', script: """parent.refreshTab({
                        tabId:'BS100-${result.bean?.id}',
                        src: '${createLink(controller: 'bs100',action: 'editPage',params: [id:result.bean?.id])}'
                    }
                );"""]
            result.scriptArrays = scriptArrays
        }

        render result as JSON
    }

    /**
     * 刪除
     * @return
     */
    JSON deleteBsRequestMap(){
        LinkedHashMap result = bsRequestMapService.delete(params)
        List scriptArrays = []
        if(!result.actionType){
            result.actionMessage = g.render(template: '/system/base/errorMessage' , model: [beanI:result.bean])
        }else{
            scriptArrays << [mode: 'execute', script: """parent.closeTab('BS100-${result.bean?.id}');"""]
            result.scriptArrays = scriptArrays
        }
        render result as JSON

    }
}
