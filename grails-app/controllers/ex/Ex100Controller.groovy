package ex

import grails.converters.JSON

class Ex100Controller {

    Ex100Service ex100Service

    /**
     * page: 程式首頁
     * @return
     */
    def index = {
        render view: "/ex/ex100/index"
    }

    /**
     * page: 新增頁面
     */
    def creatPage = {
        Ex100 ex100I = new Ex100()
        render view: "/ex/ex100/creatPage", model: [ex100I:ex100I]
    }

    /**
     * page: 編輯頁面
     */
    def editPage = {
        Ex100 ex100I = Ex100.get(params.id as long)
        render view: "/ex/ex100/editPage", model: [ex100I:ex100I]
    }

    /**
     * 查詢
     * @return
     */
    JSON filterEx100(){
        LinkedHashMap result = ex100Service.filter()
        render result as JSON
    }

    /**
     * 新增
     * @return
     */
    JSON insertEx100(){
        LinkedHashMap result = ex100Service.insert(params)
        if(!result.actionType){
            result.actionMessage = g.render(template: '/system/base/errorMessage' , model: [beanI:result.bean])
        }
        else{
            //更新tab資訊

            List scriptArrays = []
            scriptArrays << [mode: 'execute', script: """parent.openTab(
                    'EX100',
                    'EX100-${result.bean?.id}',
                    '${result.bean?.strings?:""}',
                    '${createLink(controller: 'ex100',action: 'editPage',params: [id:result.bean?.id])}'
                );"""]
            scriptArrays << [mode: 'execute', script: """parent.closeTab('EX100-0');"""]
            result.scriptArrays = scriptArrays
        }

        render result as JSON
    }

    /**
     * 更新資料
     * @return
     */
    JSON updateEx100(){
        LinkedHashMap result = ex100Service.update(params)
        if(!result.actionType){
            result.actionMessage = g.render(template: '/system/base/errorMessage' , model: [beanI:result.bean])
        }
        else{
            //更新tab資訊

            List scriptArrays = []
            scriptArrays << [mode: 'execute', script: """parent.refreshTab({
                        tabId:'EX100-${result.bean?.id}',
                        src: '${createLink(controller: 'ex100',action: 'editPage',params: [id:result.bean?.id])}'
                    }
                );"""]
            result.scriptArrays = scriptArrays
        }

        render result as JSON
    }

    /**
     * 刪除
     * @return
     */
    JSON deleteEx100(){
        LinkedHashMap result = ex100Service.delete(params)
        List scriptArrays = []
        if(!result.actionType){
            result.actionMessage = g.render(template: '/system/base/errorMessage' , model: [beanI:result.bean])
        }else{
            scriptArrays << [mode: 'execute', script: """parent.closeTab('EX100-${result.bean?.id}');"""]
            result.scriptArrays = scriptArrays
        }
        render result as JSON

    }

}
