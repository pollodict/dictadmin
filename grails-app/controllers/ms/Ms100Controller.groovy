package ms

import grails.converters.JSON

class Ms100Controller {

    Ms100Service ms100Service

    def index =  {
        render view: "/ms/ms100/index"
    }

    def creatPage = {
        Ms100 ms100I = new Ms100()
        render view: "/ms/ms100/creatPage", model: [ms100I:ms100I]
    }

    def editPage = {
        Ms100 ms100I = Ms100.get(params as long)
        render view: "/ms/ms100/editPage", model: [ms100I:ms100I]
    }

    /**
     * 查詢
     * @return
     */
    JSON filterMs100(){
        LinkedHashMap result = ms100Service.filter()
        render result as JSON
    }

    /**
     * 新增
     * @return
     */
    JSON insertMs100(){
        LinkedHashMap result = ms100Service.insert(params)
        if(!result.actionType){
            result.actionMessage = g.render(template: '/system/base/errorMessage' , model: [beanI:result.bean])
        }
        else{
            //更新tab資訊

            List scriptArrays = []
            scriptArrays << [mode: 'execute', script: """parent.openTab(
                    'MS100',
                    'MS100-${result.bean?.id}',
                    '${result.bean?.accountName?:""}',
                    '${createLink(controller: 'ms100',action: 'editPage',params: [id:result.bean?.id])}'
                );"""]
            scriptArrays << [mode: 'execute', script: """parent.closeTab('MS100-0');"""]
            result.scriptArrays = scriptArrays
        }

        render result as JSON
    }

    /**
     * 更新資料
     * @return
     */
    JSON updateMs100(){
        LinkedHashMap result = ms100Service.update(params)
        if(!result.actionType){
            result.actionMessage = g.render(template: '/system/base/errorMessage' , model: [beanI:result.bean])
        }
        else{
            //更新tab資訊

            List scriptArrays = []
            scriptArrays << [mode: 'execute', script: """parent.refreshTab({
                        tabId:'MS100-${result.bean?.id}',
                        src: '${createLink(controller: 'ms100',action: 'editPage',params: [id:result.bean?.id])}'
                    }
                );"""]
            result.scriptArrays = scriptArrays
        }

        render result as JSON
    }

    /**
     * 刪除
     * @return
     */
    JSON deleteMs100(){
        LinkedHashMap result = ms100Service.delete(params)
        List scriptArrays = []
        if(!result.actionType){
            result.actionMessage = g.render(template: '/system/base/errorMessage' , model: [beanI:result.bean])
        }else{
            scriptArrays << [mode: 'execute', script: """parent.closeTab('MS100-${result.bean?.id}');"""]
            result.scriptArrays = scriptArrays
        }
        render result as JSON

    }
}
