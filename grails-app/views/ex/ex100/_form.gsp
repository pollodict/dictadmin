<%@page import="bs.*" %>
<g:hiddenField name="ex100.version" value="${ex100I.version}" />
<app:formTable
        tableTitle="範例資料"
>
    <tr>
        <th>
            ${message(code: "ex100.strings.label")}
        </th>
        <td>
            <dict:textField name="ex100.strings" value="${ex100I.strings}" />
        </td>
        <th>
            ${message(code: "ex100.integers.label")}
        </th>
        <td>
            <dict:numberField name="ex100.integers" value="${ex100I.integers}" />
        </td>
    </tr>
    <tr>
        <th>
            ${message(code: "ex100.atms.label")}
        </th>
        <td>
            <dict:numberField name="ex100.atms" value="${ex100I.atms}" />
        </td>
        <th>
            ${message(code: "ex100.status.label")}
        </th>
        <td>
%{--            <dict:textField name="ex100.status" value="${ex100I.status}" />--}%
            <bootstrap:select name="ex100.status" value="${ex100I.status}"
                              from="${Bs101.findAllByPtype('EX100_STATUS')}"
                              optionValue="pdesc" optionKey="pcode"
                              noSelection="['': '---']"
            />
        </td>
    </tr>
    <tr>
        <th>
            ${message(code: "ex100.article.label")}
        </th>
        <td colspan="3">
            <dict:textarea name="ex100.article" value="${ex100I.article}" />
        </td>
    </tr>
</app:formTable>