<table
    id="ex100-filter"
    data-toggle="table"
    data-pagination="true"
>
    <thead>
    <tr>
        <th data-formatter="indexFormatter" data-align="center" data-width="50">#</th>
        <th data-field="id" data-formatter="linkFormatter" data-width="50">${message(code: "ex100.id.label")}</th>
        <th data-field="strings">${message(code: "ex100.strings.label")}</th>
        <th data-field="integers">${message(code: "ex100.integers.label")}</th>
        <th data-field="atms">${message(code: "ex100.atms.label")}</th>
        <th data-field="status">${message(code: "ex100.status.label")}</th>
    </tr>
    </thead>
</table>
<script type="text/javascript">


    /**
     * 顯示編輯按鈕
     * @param value
     * @param row
     * @returns {string}
     */
    function linkFormatter(value, row) {
        return '<a ' +
            'class="btn btn-link"'+
            'data-dict="openTab"'+
            'data-tab-id="'+row.id+'"'+
            'data-tab-title="'+row.strings+'"'+
            'data-url="${createLink(controller: "ex100",action: "editPage")}/' + row.id+'"'+
            '>' +
            value +
            '</a>';
    }
</script>