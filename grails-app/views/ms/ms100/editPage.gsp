<%--
  Created by IntelliJ IDEA.
  User: jameschang
  Date: 2/8/22
  Time: 11:02 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="appMain"/>
</head>

<body>
<div class="container-fluid">
    <div class="row">
        <div class="col">
            <form id="ms100-form" enctype="multipart/form-data">
                <g:render template="/domain/forms/ms/ms100/form" model="[ms100I:ms100I]" />
            </form>
            <g:render template="/bs/dictAdmin/domainMessage" model="[instance:ms100I]" />
        </div>
    </div>

    <div class="row">
        <div class="col">

            <div class="btn-group" role="group">
                <dict:button
                        data-dict="ajax"
                        data-form-id="ms100-form"
                        data-url="${createLink(controller: "ms100",action: "updateMs100" ,id: ms100I?.id)}"
                >
                    ${message(code: "default.button.save.label")}
                </dict:button>
                <dict:button
                        class="btn-danger"
                        data-dict="ajax"
                        data-form-id="ms100-form"
                        data-url="${createLink(controller: "ms100",action: "deleteMs100" ,id: ms100I?.id)}"
                        data-confirm="確定刪除？"
                >
                    ${message(code: "default.button.delete.label")}
                </dict:button>
            </div>

        </div>
    </div>
</div>
</body>
</html>