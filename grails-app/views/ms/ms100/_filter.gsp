
<!--
@author : PDMan v3.5.5
@date : 2022-2-8
@desc : 帳本
-->
<table
        id="ms100-filter"
        data-toggle="table"
        data-pagination="true"
>
    <thead>
    <tr>
        <th data-formatter="indexFormatter" data-align="center" data-width="50">#</th>
        <th data-field="id" data-formatter="linkFormatter" data-width="50">${message(code: "ms100.id.label")}</th>
        <th data-field="account">${message(code: "ms100.account.label")}</th>
        <th data-field="accountName">${message(code: "ms100.accountName.label")}</th>
    </tr>
    </thead>
</table>
<script type="text/javascript">
    /**
     * 顯示編輯按鈕
     * @param value
     * @param row
     * @returns {string}
     */
    function linkFormatter(value, row) {
        return '<a ' +
            'class="btn btn-link"'+
            'data-dict="openTab"'+
            'data-tab-id="'+row.id+'"'+
            'data-tab-title="'+row.accountName+'"'+
            'data-url="${createLink(controller: "ms100",action: "editPage")}/' + row.id+'"'+
            '>' +
            value +
            '</a>';
    }
</script>