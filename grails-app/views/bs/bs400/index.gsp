<%--
  Created by IntelliJ IDEA.
  User: jameschang
  Date: 2021/7/23
  Time: 下午9:26
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html lang="zh-Hant-TW">
<head>
    <meta name="layout" content="main"/>
</head>

<body>
<div class="container-fluid">
    <div class="row flex-xl-nowrap">
        <g:render template="/bs/dictAdmin/nav" model="[groupL:groupL,appL:appL]" />
        <g:render template="/bs/dictAdmin/main" />
    </div>
</div>
</body>
</html>