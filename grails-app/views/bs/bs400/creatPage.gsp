<%--
  新增頁面
  Created by IntelliJ IDEA.
  User: jameschang
  Date: 9/5/21
  Time: 11:16 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="appMain"/>
</head>

<body>
<div class="container-fluid">
    <div class="row">
        <div class="col">
            <form id="ex100-form" enctype="multipart/form-data">
                <g:render template="/ex/ex100/form" model="[ex100I:ex100I]" />
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col">

            <div class="btn-group" role="group">
                <dict:button
                        data-dict="ajax"
                        data-form-id="ex100-form"
                        data-url="${createLink(controller: "ex100",action: "insertEx100")}"
                >
                    ${message(code: "default.button.creat.label")}
                </dict:button>
            </div>

        </div>
    </div>
</div>
</body>
</html>