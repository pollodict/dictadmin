<g:hiddenField name="ex100.version" value="${ex100I.version}" />
<table class="table table-bordered">
    <colgroup>
        <col width="15%">
        <col width="35%">
        <col width="15%">
        <col width="35%">
    </colgroup>
    <thead>
    <tr>
        <th colspan="4">
            <asset:image src="/icons/border-width.svg"/>
            新增資料
        </th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th>
            ${message(code: "ex100.strings.label")}
        </th>
        <td>
            <dict:textField name="ex100.strings" value="${ex100I.strings}" />
        </td>
        <th>
            ${message(code: "ex100.integers.label")}
        </th>
        <td>
            <dict:numberField name="ex100.integers" value="${ex100I.integers}" />
        </td>
    </tr>
    <tr>
        <th>
            ${message(code: "ex100.atms.label")}
        </th>
        <td>
            <dict:numberField name="ex100.atms" value="${ex100I.atms}" />
        </td>
        <th>
            ${message(code: "ex100.status.label")}
        </th>
        <td>
            <dict:textField name="ex100.status" value="${ex100I.status}" />
        </td>
    </tr>
    <tr>
        <th>
            ${message(code: "ex100.article.label")}
        </th>
        <td colspan="3">
            <dict:textarea name="ex100.article" value="${ex100I.article}" />
        </td>
    </tr>
    </tbody>
</table>