<g:hiddenField name="bsRequestMap.version" value="${bsRequestMapI.version}" />
<table class="table table-bordered">
    <colgroup>
        <col width="15%">
        <col width="35%">
        <col width="15%">
        <col width="35%">
    </colgroup>
    <thead>
    <tr>
        <th colspan="4">
            <asset:image src="/icons/border-width.svg"/>
            新增資料
        </th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th>
            ${message(code: "bsRequestMap.appNo.label")}
        </th>
        <td>
            <dict:textField name="bsRequestMap.appNo" value="${bsRequestMapI.appNo}" />
        </td>
        <th>
            ${message(code: "bsRequestMap.appName.label")}
        </th>
        <td>
            <dict:textField name="bsRequestMap.appName" value="${bsRequestMapI.appName}" />
        </td>
    </tr>
    <tr>
        <th>
            ${message(code: "bsRequestMap.idx.label")}
        </th>
        <td>
            <dict:numberField name="bsRequestMap.idx" value="${bsRequestMapI.idx}" />
        </td>
        <th>
            ${message(code: "bsRequestMap.isShow.label")}
        </th>
        <td>
            是<g:radio name="bsRequestMap.isShow" value="true" checked="${bsRequestMapI.isShow}" />
            否<g:radio name="bsRequestMap.isShow" value="false" checked="${!bsRequestMapI.isShow}" />
        </td>
    </tr>
    <tr>
        <th>
            ${message(code: "bsRequestMap.configAttribute.label")}
        </th>
        <td>
            <dict:textField name="bsRequestMap.configAttribute" value="${bsRequestMapI.configAttribute}" />
        </td>
        <th>
            ${message(code: "bsRequestMap.httpMethod.label")}
        </th>
        <td>
            <dict:textField name="bsRequestMap.httpMethod" value="${bsRequestMapI.httpMethod}" />
        </td>
    </tr>
    <tr>
        <th>
            ${message(code: "bsRequestMap.url.label")}
        </th>
        <td colspan="3">
            <dict:textField name="bsRequestMap.url" value="${bsRequestMapI.url}" />
        </td>
    </tr>
    </tbody>
</table>