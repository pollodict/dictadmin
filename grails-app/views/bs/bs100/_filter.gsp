<app:filterTable
        id="${filterID}"
>
    <tr>
        <th data-formatter="indexFormatter" data-align="center" data-width="50">#</th>
        <th data-field="id" data-formatter="linkFormatter" data-width="50">${message(code: "bsRequestMap.appNo.label")}</th>
        <th data-field="appName">${message(code: "bsRequestMap.appName.label")}</th>
        <th data-field="idx">${message(code: "bsRequestMap.idx.label")}</th>
        <th data-field="isShow">${message(code: "bsRequestMap.isShow.label")}</th>
        <th data-field="configAttribute">${message(code: "bsRequestMap.configAttribute.label")}</th>
        <th data-field="httpMethod">${message(code: "bsRequestMap.httpMethod.label")}</th>
        <th data-field="url">${message(code: "bsRequestMap.url.label")}</th>
    </tr>
</app:filterTable>
<script type="text/javascript">


    /**
     * 顯示編輯按鈕
     * @param value
     * @param row
     * @returns {string}
     */
    function linkFormatter(value, row) {
        return '<a ' +
            'class="btn btn-link"'+
            'data-dict="openTab"'+
            'data-tab-id="'+row.id+'"'+
            'data-tab-title="'+row.appNo+'"'+
            'data-url="${createLink(controller: "bs100",action: "editPage")}/' + row.id+'"'+
            '>' +
            row.appNo +
            '</a>';
    }
</script>