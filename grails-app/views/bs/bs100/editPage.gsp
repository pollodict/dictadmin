<%--
  編輯頁面
  Created by IntelliJ IDEA.
  User: jameschang
  Date: 9/5/21
  Time: 11:16 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="appMain"/>
</head>

<body>
<div class="container-fluid">
    <div class="row">
        <div class="col">
            <form id="bs100-form" enctype="multipart/form-data">
                <g:render template="/bs/bs100/form" model="[bsRequestMapI:bsRequestMapI]" />
            </form>
            <g:render template="/bs/dictAdmin/domainMessage" model="[instance:bsRequestMapI]" />
        </div>
    </div>

    <div class="row">
        <div class="col">

            <div class="btn-group" role="group">
                <dict:button
                        data-dict="ajax"
                        data-form-id="bs100-form"
                        data-url="${createLink(controller: "bs100",action: "updateBsRequestMap" ,id: bsRequestMapI?.id)}"
                >
                    ${message(code: "default.button.save.label")}
                </dict:button>
                <dict:button
                        class="btn-danger"
                        data-dict="ajax"
                        data-form-id="bs100-form"
                        data-url="${createLink(controller: "bs100",action: "deleteBsRequestMap" ,id: bsRequestMapI?.id)}"
                        data-confirm="確定刪除？"
                >
                    ${message(code: "default.button.delete.label")}
                </dict:button>
            </div>

        </div>
    </div>
</div>
</body>
</html>