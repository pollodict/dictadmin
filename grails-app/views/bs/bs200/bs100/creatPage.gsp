<%--
  新增頁面
  Created by IntelliJ IDEA.
  User: jameschang
  Date: 9/5/21
  Time: 11:16 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="appMain"/>
</head>

<body>
<div class="container-fluid">
    <div class="row">
        <div id="system-message" class="col">
        </div>
    </div>
    <div class="row">
        <div class="col">
            <form id="bs100-form" enctype="multipart/form-data">
                <g:render template="/bs/bs200/bs100/form" model="[bs100I:bs100I]" />
            </form>
        </div>
    </div>

    <div class="row">
        <div class="col">

            <div class="btn-group" role="group">
                <dict:button
                        data-dict="ajax"
                        data-form-id="bs100-form"
                        data-url="${createLink(controller: "bs200",action: "insertBs100")}"
                >
                    ${message(code: "default.button.creat.label")}
                </dict:button>
            </div>

        </div>
    </div>
</div>
</body>
</html>