<g:hiddenField name="bs100.version" value="${bs100I.version}" />
<app:formTable
        tableTitle="系統選單類別資訊"
>
    <tr>
        <th>
            ${message(code: "bs100.systemType.label")}
        </th>
        <td colspan="3">
            <dict:textField name="bs100.systemType" value="${bs100I.systemType}" />
        </td>
    </tr>
    <tr>
        <th>
            ${message(code: "bs100.ptype.label")}
        </th>
        <td>
            <dict:textField class="form-control" name="bs100.ptype" value="${bs100I.ptype}" uppercase="true" onlyNumberAndLetter="true"  />

        </td>
        <th>
            ${message(code: "bs100.ptypeDesc.label")}
        </th>
        <td>
            <dict:textField name="bs100.ptypeDesc" value="${bs100I.ptypeDesc}" />
        </td>
    </tr>
</app:formTable>