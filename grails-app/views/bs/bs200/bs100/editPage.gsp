<%--
  編輯頁面
  Created by IntelliJ IDEA.
  User: jameschang
  Date: 9/5/21
  Time: 11:16 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="appMain"/>
</head>

<body>
<app:editContainer>
    <app:editContainerTabs
            tabId="bs-app-group-tab"
            tabList="${[
                    [tabId:"tab-1",active:true,tabName:"選單類別",url:createLink(controller: "bs200",action: "editBs100", params: [id:bs100I?.id])],
                    [tabId:"tab-2",active:false,tabName:"選項",url:createLink(controller: "bs200",action: "bs101List", params: [id:bs100I?.id])],
            ]}"
    />
</app:editContainer>
<!-- Modal -->
<bootstrap:modal modelId="data-model"/>
</body>
</html>