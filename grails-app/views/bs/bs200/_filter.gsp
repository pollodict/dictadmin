<table
    id="bs200-filter"
    data-toggle="table"
    data-pagination="true"
>
    <thead>
    <tr>
        <th data-formatter="indexFormatter" data-align="center" data-width="50">#</th>
        <th data-field="id" data-formatter="linkFormatter" data-width="50">${message(code: "bs100.ptype.label")}</th>
        <th data-field="systemType">${message(code: "bs100.systemType.label")}</th>
        <th data-field="ptypeDesc">${message(code: "bs100.ptypeDesc.label")}</th>
    </tr>
    </thead>
</table>
<script type="text/javascript">


    /**
     * 顯示編輯按鈕
     * @param value
     * @param row
     * @returns {string}
     */
    function linkFormatter(value, row) {
        return '<a ' +
            'class="btn btn-link"'+
            'data-dict="openTab"'+
            'data-tab-id="'+row.id+'"'+
            'data-tab-title="'+row.ptype+'"'+
            'data-url="${createLink(controller: "bs200",action: "editPageBs100")}/' + row.id+'"'+
            '>' +
            row.ptype +
            '</a>';
    }
</script>