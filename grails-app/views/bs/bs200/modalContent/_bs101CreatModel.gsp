<g:set var="formName" value="bs101-form" />
<bootstrap:modelHeader modelTitle="${modelTitle}" />
<bootstrap:modelBody formName="${formName}">
        <g:hiddenField name="modelId" value="${modelId}" />
        <g:hiddenField name="modelTitle" value="${modelTitle}" />
        <g:render template="/domain/forms/bs/bs101/form" model="[bs101I:bs101I]" />
</bootstrap:modelBody>
<bootstrap:modelFooter>
    <dict:button
            data-dict="ajax"
            data-form-id="${formName}"
            data-url="${createLink(controller: "bs200",action: "insertBs101"
            )}"
    >
        ${message(code: "default.button.save.label")}
    </dict:button>
</bootstrap:modelFooter>