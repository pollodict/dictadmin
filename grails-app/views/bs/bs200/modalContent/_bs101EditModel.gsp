<g:set var="formName" value="bs101-form" />
<bootstrap:modelHeader modelTitle="${modelTitle}" />
<bootstrap:modelBody formName="${formName}">
    <g:hiddenField name="modelId" value="${modelId}" />
    <g:hiddenField name="modelTitle" value="${modelTitle}" />
    <g:render template="/domain/forms/bs/bs101/form" model="[bs101I:bs101I]" />
    <g:render template="/bs/dictAdmin/domainMessage" model="[instance:bs101I]" />
</bootstrap:modelBody>
<bootstrap:modelFooter>
    <dict:button
            data-dict="ajax"
            data-form-id="${formName}"
            data-url="${createLink(controller: "bs200",action: "updateBs101" ,
                    id: bs101I?.id
            )}"
    >
        ${message(code: "default.button.save.label")}
    </dict:button>
    <dict:button
            class="btn-danger"
            data-dict="ajax"
            data-form-id="${formName}"
            data-url="${createLink(controller: "bs200",action: "deleteBs101" ,
                    id: bs101I?.id
            )}"
            data-confirm="${message(code: "default.button.delete.confirm.message")}"
    >
        ${message(code: "default.button.delete.label")}
    </dict:button>
</bootstrap:modelFooter>