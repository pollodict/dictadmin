<g:set var="filterName" value="bs101-filter" />
<app:editContainer>
    <g:form id="search-form-bsAppListGroup">
        <app:searchTable>
            <tbody>
            <tr>
                <th>
                    ${message(code: "bs101.pdesc.label")}
                </th>
                <td>
                    <dict:textField name="pdesc" />
                </td>
                <th>
                    ${message(code: "bs101.pcode.label")}
                </th>
                <td>
                    <dict:textField name="pcode" />
                </td>
            </tr>
            </tbody>

            <tfoot>
            <tr>
                <td colspan="4">
                    <div class="btn-group" role="group">
                        <dict:button
                                onclick="filterSearchBsAppListGroup();"
                        >
                            ${message(code: "default.button.search.label")}
                        </dict:button>

                        <dict:button
                                class="btn-outline-secondary"
                        >
                            ${message(code: "default.button.reset.label")}
                        </dict:button>

                        <dict:button
                                class="btn-light btn-outline-dark"
                        >
                            ${message(code: "default.button.print.label")}
                        </dict:button>
                    </div>

                    <div class="btn-group" role="group">
                        <dict:button
                                class="btn-success"
                                data-dict="openModel"
                                data-model-id="data-model"
                                data-model-title="新增"
                                data-url="${createLink(controller: "bs200",action: "creatBs101",params: [id:bs100I?.id])}"
                        >
                            ${message(code: "default.button.creat.label")}
                        </dict:button>
                    </div>
                </td>
            </tr>
            </tfoot>
        </app:searchTable>

        <app:searchFilter>
            <g:render template="/bs/bs200/tabs/bs101List/filter" model="[filterID:filterName,bsAppGroupI:bs100I]" />
        </app:searchFilter>
    </g:form>
</app:editContainer>
<script type="text/javascript">
    function filterSearchBsAppListGroup(){
        jQuery('#${filterName}').bootstrapTable('refresh', {
            url: '${createLink(controller: 'bs200' ,action: 'bs101Filter')}/?bs100Id=${bs100I?.id}&' + jQuery('#search-form-bsAppListGroup').serialize()
        });
    }

    jQuery(function(){
        filterSearchBsAppListGroup();
    });
</script>