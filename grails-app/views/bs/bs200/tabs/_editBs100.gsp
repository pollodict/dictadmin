<app:editContainer>
    <app:editContainerBody
            formId="bs110-form"
            instance="${bs100I}">
        <g:render template="/bs/bs200/bs100/form" model="[bs100I:bs100I]" />
    </app:editContainerBody>

    <app:editContainerFooter>

        <div class="btn-group" role="group">
            <dict:button
                    data-dict="ajax"
                    data-form-id="bs110-form"
                    data-url="${createLink(controller: "bs200",action: "updateBs100" ,id: bs100I?.id)}"
            >
                ${message(code: "default.button.save.label")}
            </dict:button>
            <dict:button
                    class="btn-danger"
                    data-dict="ajax"
                    data-form-id="bs110-form"
                    data-url="${createLink(controller: "bs200",action: "deleteBs100" ,id: bs100I?.id)}"
                    data-confirm="${message(code: "default.button.delete.confirm.message")}"
            >
                ${message(code: "default.button.delete.label")}
            </dict:button>
        </div>

    </app:editContainerFooter>
</app:editContainer>