<app:filterTable
        id="${filterID}"
>
    <tr>
        <th data-formatter="indexFormatter" data-align="center" data-width="50">#</th>
        <th data-field="id" data-formatter="linkFormatter" data-width="50">${message(code: "bs101.pcode.label")}</th>
        <th data-field="pdesc">${message(code: "bs101.pdesc.label")}</th>
        <th data-field="existsString1">${message(code: "bs101.existsString1.label")}</th>
        <th data-field="existsString2">${message(code: "bs101.existsString2.label")}</th>
        <th data-field="existsNumber1">${message(code: "bs101.existsNumber1.label")}</th>
        <th data-field="existsNumber2">${message(code: "bs101.existsNumber2.label")}</th>

    </tr>
</app:filterTable>
<script type="text/javascript">


    /**
     * 顯示編輯按鈕
     * @param value
     * @param row
     * @returns {string}
     */
    function linkFormatter(value, row) {
        return '<a ' +
            'class="btn btn-link"'+
            'data-dict="openModel"'+
            'data-model-id="data-model"'+
            'data-model-title="'+row.pcode+'"'+
            'data-url="${createLink(controller: "bs200",action: "editBs101")}/' + row.id+'"'+
            '>' +
            row.pcode +
            '</a>';
    }
</script>