<%--
  Created by IntelliJ IDEA.
  User: jameschang
  Date: 9/1/21
  Time: 11:31 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html lang="zh-Hant-TW">
<head>
    <meta name="layout" content="appMain"/>
    <script type="text/javascript">
        function filterSearch(){
            jQuery('#bs200-filter').bootstrapTable('refresh', {
                url: '${createLink(controller: 'bs200' ,action: 'filterBs100')}/?' + jQuery('#search-form').serialize()
            });
        }
    </script>
</head>

<body>
<div class="container-fluid">
    <div class="row">
        <div id="system-message" class="col">
        </div>
    </div>
    <form id="search-form">
        <div class="row">
            <div class="col">
                <table class="table table-bordered">
                    <colgroup>
                        <col width="15%">
                        <col width="35%">
                        <col width="15%">
                        <col width="35%">
                    </colgroup>
                    <thead>
                    <tr>
                        <th colspan="4">
                            <asset:image src="/icons/border-width.svg"/>
                            查詢條件
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th>
                            ${message(code: "bs100.systemType.label")}
                        </th>
                        <td>
                            <dict:textField name="systemType" />
                        </td>
                        <th>
                            ${message(code: "bs100.ptype.label")}
                        </th>
                        <td>
                            <dict:textField name="ptype"/>
                        </td>
                    </tr>
                    </tbody>
                    <tfoot>
                    <tr>
                        <td colspan="4">
                            <div class="btn-group" role="group">
                                <dict:button
                                        onclick="filterSearch();"
                                >
                                    ${message(code: "default.button.search.label")}
                                </dict:button>

                                <dict:button
                                        class="btn-outline-secondary"
                                >
                                    ${message(code: "default.button.reset.label")}
                                </dict:button>

                                <dict:button
                                        class="btn-light btn-outline-dark"
                                >
                                    ${message(code: "default.button.print.label")}
                                </dict:button>
                            </div>

                            <div class="btn-group" role="group">
                                <dict:button
                                        class="btn-success"
                                        data-dict="openTab"
                                        data-tab-id="0"
                                        data-tab-title="新增"
                                        data-url="${createLink(controller: "bs200",action: "creatPageBS100")}"
                                >
                                    ${message(code: "default.button.creat.label")}
                                </dict:button>
                            </div>
                        </td>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </div>

        <div class="row">
            <div class="col">
                <asset:image src="/icons/border-width.svg"/>
                查詢結果
            </div>
        </div>

        <div class="row">
            <div class="col">
                <g:render template="/bs/bs200/filter" />
            </div>
        </div>
    </form>
</div>
</body>
</html>