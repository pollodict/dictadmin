<bootstrap:modelHeader modelTitle="${modelTitle}" />
<bootstrap:modelBody>
    <g:form name="bs-app-list-group-form">
        <g:hiddenField name="modelId" value="${modelId}" />
        <g:hiddenField name="modelTitle" value="${modelTitle}" />
        <g:render template="/domain/forms/bs/bsAppListGroup/form" model="[bsAppListGroupI:bsAppListGroupI]" />
    </g:form>
</bootstrap:modelBody>
<bootstrap:modelFooter>
    <dict:button
            data-dict="ajax"
            data-form-id="bs-app-list-group-form"
            data-url="${createLink(controller: "bs110",action: "insertsAppListGroup"
            )}"
    >
        ${message(code: "default.button.save.label")}
    </dict:button>
</bootstrap:modelFooter>