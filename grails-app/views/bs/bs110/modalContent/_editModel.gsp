<bootstrap:modelHeader modelTitle="${modelTitle}" />
<bootstrap:modelBody>
    <g:form name="bs-app-list-group-form">
        <g:hiddenField name="modelId" value="${modelId}" />
        <g:hiddenField name="modelTitle" value="${modelTitle}" />
        <g:render template="/domain/forms/bs/bsAppListGroup/form" model="[bsAppListGroupI:bsAppListGroupI]" />
    </g:form>
    <g:render template="/bs/dictAdmin/domainMessage" model="[instance:bsAppListGroupI]" />
</bootstrap:modelBody>
<bootstrap:modelFooter>
    <dict:button
            data-dict="ajax"
            data-form-id="bs-app-list-group-form"
            data-url="${createLink(controller: "bs110",action: "updateBsAppListGroup" ,
                    id: bsAppListGroupI?.id
            )}"
    >
        ${message(code: "default.button.save.label")}
    </dict:button>
    <dict:button
            class="btn-danger"
            data-dict="ajax"
            data-form-id="bs-app-list-group-form"
            data-url="${createLink(controller: "bs110",action: "deleteBsAppListGroup" ,id: bsAppListGroupI?.id)}"
            data-confirm="${message(code: "default.button.delete.confirm.message")}"
    >
        ${message(code: "default.button.delete.label")}
    </dict:button>
</bootstrap:modelFooter>