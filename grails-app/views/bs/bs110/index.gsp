<%--
  Created by IntelliJ IDEA.
  User: jameschang
  Date: 9/1/21
  Time: 11:31 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html lang="zh-Hant-TW">
<head>
    <meta name="layout" content="appMain"/>
    <script type="text/javascript">
        function filterSearch(){
            jQuery('#bs110-filter').bootstrapTable('refresh', {
                url: '${createLink(controller: 'bs110' ,action: 'filter')}/?' + jQuery('#search-form').serialize()
            });
        }
    </script>
</head>

<body>
<div class="container-fluid">
    <form id="search-form">
        <app:searchTable>
            <tbody>
            <tr>
                <th>
                    ${message(code: "bsAppGroup.appGroupNo.label")}
                </th>
                <td>
                    <dict:textField name="appGroupNo" />
                </td>
                <th>
                    ${message(code: "bsAppGroup.appGroupName.label")}
                </th>
                <td>
                    <dict:textField name="appGroupName" />
                </td>
            </tr>
            </tbody>
            <tfoot>
            <tr>
                <td colspan="4">
                    <div class="btn-group" role="group">
                        <dict:button
                                onclick="filterSearch();"
                        >
                            ${message(code: "default.button.search.label")}
                        </dict:button>

                        <dict:button
                                class="btn-outline-secondary"
                        >
                            ${message(code: "default.button.reset.label")}
                        </dict:button>

                        <dict:button
                                class="btn-light btn-outline-dark"
                        >
                            ${message(code: "default.button.print.label")}
                        </dict:button>
                    </div>

                    <div class="btn-group" role="group">
                        <dict:button
                                class="btn-success"
                                data-dict="openTab"
                                data-tab-id="0"
                                data-tab-title="新增"
                                data-url="${createLink(controller: "bs110",action: "creatPage")}"
                        >
                            ${message(code: "default.button.creat.label")}
                        </dict:button>
                    </div>
                </td>
            </tr>
            </tfoot>
        </app:searchTable>

        <app:searchFilter>
            <g:render template="/bs/bs110/filter" model="[filterID:'bs110-filter']" />
        </app:searchFilter>
    </form>
</div>
</body>
</html>