<g:hiddenField name="bsAppGroup.version" value="${bsAppGroupI.version}" />
<app:formTable
    tableTitle="${message(code: "bsAppGroup.table.label")}"
>
    <tr>
        <th>
            ${message(code: "bsAppGroup.appGroupNo.label")}
        </th>
        <td>
            <dict:textField name="bsAppGroup.appGroupNo" value="${bsAppGroupI.appGroupNo}" />
        </td>
        <th>
            ${message(code: "bsAppGroup.appGroupName.label")}
        </th>
        <td>
            <dict:textField name="bsAppGroup.appGroupName" value="${bsAppGroupI.appGroupName}" />
        </td>
    </tr>
</app:formTable>