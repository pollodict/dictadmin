<app:filterTable
        id="${filterID}"
>
    <tr>
        <th data-formatter="indexFormatter" data-align="center" data-width="50">#</th>
        <th data-field="id" data-formatter="linkFormatter" data-width="50">${message(code: "bsAppListGroup.bsAppListId.label")}</th>
        <th data-field="appName">${message(code: "bsAppGroup.appName.label")}</th>
        <th data-field="note">${message(code: "bsAppGroup.note.label")}</th>

    </tr>
</app:filterTable>
<script type="text/javascript">


    /**
     * 顯示編輯按鈕
     * @param value
     * @param row
     * @returns {string}
     */
    function linkFormatter(value, row) {
        return '<a ' +
            'class="btn btn-link"'+
            'data-dict="openModel"'+
            'data-model-id="data-model"'+
            'data-model-title="'+row.appNo+'"'+
            'data-url="${createLink(controller: "bs110",action: "editPageBsAppListGroup")}/' + row.id+'"'+
            '>' +
            row.appNo +
            '</a>';
    }
</script>