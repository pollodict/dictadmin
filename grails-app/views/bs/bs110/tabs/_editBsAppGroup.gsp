<app:editContainer>
    <app:editContainerBody
            formId="bs110-form"
            instance="${bsAppGroupI}">
        <g:render template="/bs/bs110/form" model="[bsAppGroupI:bsAppGroupI]" />
    </app:editContainerBody>

    <app:editContainerFooter>

        <div class="btn-group" role="group">
            <dict:button
                    data-dict="ajax"
                    data-form-id="bs110-form"
                    data-url="${createLink(controller: "bs110",action: "update" ,id: bsAppGroupI?.id)}"
            >
                ${message(code: "default.button.save.label")}
            </dict:button>
            <dict:button
                    class="btn-danger"
                    data-dict="ajax"
                    data-form-id="bs110-form"
                    data-url="${createLink(controller: "bs110",action: "delete" ,id: bsAppGroupI?.id)}"
                    data-confirm="${message(code: "default.button.delete.confirm.message")}"
            >
                ${message(code: "default.button.delete.label")}
            </dict:button>
        </div>

    </app:editContainerFooter>
</app:editContainer>