<div class="container-fluid">
    <form id="search-form-bsAppListGroup">
        <app:searchTable>
            <tbody>
            <tr>
                <th>
                    ${message(code: "bsAppListGroup.bsAppListId.label")}
                </th>
                <td>
                    <dict:textField name="bsAppListId" />
                </td>
                <th>
                    ${message(code: "bsAppGroup.appName.label")}
                </th>
                <td>
                    <dict:textField name="appName" />
                </td>
            </tr>
            </tbody>

            <tfoot>
            <tr>
                <td colspan="4">
                    <div class="btn-group" role="group">
                        <dict:button
                                onclick="filterSearchBsAppListGroup();"
                        >
                            ${message(code: "default.button.search.label")}
                        </dict:button>

                        <dict:button
                                class="btn-outline-secondary"
                        >
                            ${message(code: "default.button.reset.label")}
                        </dict:button>

                        <dict:button
                                class="btn-light btn-outline-dark"
                        >
                            ${message(code: "default.button.print.label")}
                        </dict:button>
                    </div>

                    <div class="btn-group" role="group">
                        <dict:button
                                class="btn-success"
                                data-dict="openModel"
                                data-model-id="data-model"
                                data-model-title="新增"
                                data-url="${createLink(controller: "bs110",action: "creatPageBsAppListGroup",params: [appGroupNo:bsAppGroupI?.appGroupNo])}"
                        >
                            ${message(code: "default.button.creat.label")}
                        </dict:button>
                    </div>
                </td>
            </tr>
            </tfoot>
        </app:searchTable>

        <app:searchFilter>
            <g:render template="/bs/bs110/tabs/bsAppListGroup/filter" model="[filterID:'bsAppListGroup-filter',bsAppGroupI:bsAppGroupI]" />
        </app:searchFilter>
    </form>
</div>
<script type="text/javascript">
    function filterSearchBsAppListGroup(){
        jQuery('#bsAppListGroup-filter').bootstrapTable('refresh', {
            url: '${createLink(controller: 'bs110' ,action: 'bsAppListGroupFilter')}/?appGroupNo=${bsAppGroupI?.appGroupNo}&' + jQuery('#search-form-bsAppListGroup').serialize()
        });
    }

    jQuery(function(){
        filterSearchBsAppListGroup();
    });
</script>