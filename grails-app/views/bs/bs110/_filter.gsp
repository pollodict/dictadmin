<app:filterTable
        id="${filterID}"
    >
    <tr>
        <th data-formatter="indexFormatter" data-align="center" data-width="50">#</th>
        <th data-field="id" data-formatter="linkFormatter" data-width="50">${message(code: "bsAppListGroup.bsAppListId.label")}</th>
        <th data-field="appGroupName">${message(code: "bsAppGroup.appGroupName.label")}</th>
        <th data-field="note">${message(code: "bsAppGroup.note.label")}</th>

    </tr>
</app:filterTable>
<script type="text/javascript">


    /**
     * 顯示編輯按鈕
     * @param value
     * @param row
     * @returns {string}
     */
    function linkFormatter(value, row) {
        return '<a ' +
            'class="btn btn-link"'+
            'data-dict="openTab"'+
            'data-tab-id="'+row.id+'"'+
            'data-tab-title="'+row.appGroupNo+'"'+
            'data-url="${createLink(controller: "bs110",action: "editPage")}/' + row.id+'"'+
            '>' +
            row.appGroupNo +
            '</a>';
    }
</script>