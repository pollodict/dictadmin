<%--
  編輯頁面
  Created by IntelliJ IDEA.
  User: jameschang
  Date: 9/5/21
  Time: 11:16 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="appMain"/>
</head>

<body>
<app:editContainer>
    <app:editContainerBody
            formId="bs110-form"
            instance="${bsAppGroupI}">
        <g:render template="/bs/bs110/form" model="[bsAppGroupI:bsAppGroupI]" />
    </app:editContainerBody>

    <app:editContainerFooter>
        <div class="btn-group" role="group">
            <dict:button
                    data-dict="ajax"
                    data-form-id="bs110-form"
                    data-url="${createLink(controller: "bs110",action: "insert")}"
            >
                ${message(code: "default.button.creat.label")}
            </dict:button>
        </div>
    </app:editContainerFooter>
</app:editContainer>
</body>
</html>