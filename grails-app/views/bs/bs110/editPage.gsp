<%--
  編輯頁面
  Created by IntelliJ IDEA.
  User: jameschang
  Date: 9/5/21
  Time: 11:16 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="appMain"/>
</head>

<body>
<app:editContainer>
    <app:editContainerTabs
        tabId="bs-app-group-tab"
        tabList="${[
            [tabId:"tab-1",active:true,tabName:"程式群組",url:createLink(controller: "bs110",action: "editBsAppGroup", params: [id:bsAppGroupI?.id])],
            [tabId:"tab-2",active:false,tabName:"程式清單",url:createLink(controller: "bs110",action: "bsAppListGroupList", params: [id:bsAppGroupI?.id])],
            ]}"
    />
</app:editContainer>
<!-- Modal -->
<bootstrap:modal modelId="data-model"/>

</body>
</html>