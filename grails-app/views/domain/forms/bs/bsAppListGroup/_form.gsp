<g:hiddenField name="bsAppListGroup.version" value="${bsAppListGroupI.version}" />
<app:formTable
        tableTitle="${message(code: "bsAppListGroup.table.label")}"
>
    <tr>
        <th>
            ${message(code: "bsAppListGroup.appGroupNo.label")}
        </th>
        <td>
            <dict:textField name="bsAppListGroup.appGroupNo" value="${bsAppListGroupI.appGroupNo}" />
        </td>
        <th>
            ${message(code: "bsAppListGroup.appNo.label")}
        </th>
        <td>
            <bootstrap:select name="bsAppListGroup.appNo" value="${bsAppListGroupI.appNo}"
                              from="${tw.com.pollochang.security.BsRequestMap.list()}"
                              optionValue="appName" optionKey="appNo"
                              noSelection="['': '---']"
            />
        </td>
    </tr>
</app:formTable>