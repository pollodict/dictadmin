<g:hiddenField name="bs101.version" value="${bs101I.version}" />
<g:hiddenField name="bs101.bs100" value="${bs101I.bs100Id}" />
<g:hiddenField name="bs101.ptype" value="${bs101I.ptype}" />
<app:formTable
        tableTitle="${message(code: "bs101.table.label")}"
>
    <tr>
        <th>
            ${message(code: "bs101.pcode.label")}
        </th>
        <td>
            <dict:numberField name="bs101.pcode" value="${bs101I.pcode}" />
        </td>
        <th>
            ${message(code: "bs101.pdesc.label")}
        </th>
        <td>
            <dict:textField name="bs101.pdesc" value="${bs101I.pdesc}" />
        </td>
    </tr>
    <tr>
        <th>
            ${message(code: "bs101.existsString1.label")}
        </th>
        <td>
            <dict:textField name="bs101.existsString1" value="${bs101I.existsString1}" />
        </td>
        <th>
            ${message(code: "bs101.existsString2.label")}
        </th>
        <td>
            <dict:textField name="bs101.existsString2" value="${bs101I.existsString2}" />
        </td>
    </tr>
    <tr>
        <th>
            ${message(code: "bs101.existsNumber1.label")}
        </th>
        <td>
            <dict:numberField name="bs101.existsNumber1" value="${bs101I.existsNumber1}" />
        </td>
        <th>
            ${message(code: "bs101.existsNumber2.label")}
        </th>
        <td>
            <dict:numberField name="bs101.existsNumber2" value="${bs101I.existsNumber2}" />
        </td>
    </tr>
</app:formTable>