
<%--
@author : PDMan v3.5.5
@date : 2022-2-6
@desc : 收支資料
--%>
<g:hiddenField name="ms110.objid" value="${ms110I.objid}" />
<g:hiddenField name="ms110.version" value="${ms110I.version}" />
<table class="table table-bordered">
    <colgroup>
        <col width="15%">
        <col width="35%">
        <col width="15%">
        <col width="35%">
    </colgroup>
    <thead>
    <tr>
        <th colspan="4">
            <asset:image src="/icons/border-width.svg"/>
            編輯資料
        </th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th>
            ${message(code: "ms110.ms100Id.label")}
        </th>
        <td>
            <dict:textField name="ms110.ms100Id" value="${ms110I.ms100Id}" />
        </td>

        <th>
            ${message(code: "ms110.status1.label")}
        </th>
        <td>
            <dict:textField name="ms110.status1" value="${ms110I.status1}" />
        </td>
    </tr>
    <tr>
        <th>
            ${message(code: "ms110.status2.label")}
        </th>
        <td>
            <dict:textField name="ms110.status2" value="${ms110I.status2}" />
        </td>

        <th>
            ${message(code: "ms110.amt.label")}
        </th>
        <td>
            <dict:textField name="ms110.amt" value="${ms110I.amt}" />
        </td>
    </tr>
    <tr>
        <th>
            ${message(code: "ms110.remark.label")}
        </th>
        <td>
            <dict:textField name="ms110.remark" value="${ms110I.remark}" />
        </td>
    </tbody>
</table>