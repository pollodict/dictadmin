
<%--
@author : PDMan v3.5.5
@date : 2022-2-6
@desc : 帳本
--%>
<g:hiddenField name="ms100.id" value="${ms100I.id}" />
<g:hiddenField name="ms100.version" value="${ms100I.version}" />
<table class="table table-bordered">
    <colgroup>
        <col width="15%">
        <col width="35%">
        <col width="15%">
        <col width="35%">
    </colgroup>
    <thead>
    <tr>
        <th colspan="4">
            <asset:image src="/icons/border-width.svg"/>
            編輯資料
        </th>
    </tr>
    </thead>
    <tbody>
    <tr>
        <th>
            ${message(code: "ms100.account.label")}
        </th>
        <td>
            <dict:textField name="ms100.account" value="${ms100I.account}" />
        </td>

        <th>
            ${message(code: "ms100.accountName.label")}
        </th>
        <td>
            <dict:textField name="ms100.accountName" value="${ms100I.accountName}" />
        </td>
    </tr>
    </tbody>
</table>