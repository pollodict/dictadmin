<%--
  Created by IntelliJ IDEA.
  User: jameschang
  Date: 9/5/21
  Time: 8:56 AM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <asset:link rel="icon" href="icon.svg" type="image/x-ico"/>
    <asset:stylesheet src="applicationMain.css"/>
    <asset:javascript src="applicationMain.js"/>
%{--    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">--}%
    <g:layoutHead/>
%{--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">--}%
%{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>--}%

</head>

<body>
<nav aria-label="breadcrumb">
    <ol class="breadcrumb bg-secondary">
        <li class="breadcrumb-item active text-white" aria-current="page">${message(code: "${controllerName}.${actionName}.label")}[${controllerName}/${actionName}]</li>
    </ol>
</nav>
<g:hiddenField name="appControllerName" value="${controllerName.toUpperCase()}"/>
<g:layoutBody/>
</body>

</html>