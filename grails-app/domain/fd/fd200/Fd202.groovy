package fd.fd200

import fd.Fd202Fd102
import fd.fd100.Fd101


/**
 * @author : PDMan v3.5.5
 * @date : 2021-11-22
 * @desc : 顧客品項
 */
class Fd202{
    static auditable = [ignore: ['dateCreated', 'lastUpdated', 'manCreated', 'manLastUpdated']]
    static belongsTo = [fd201:Fd201]
    static hasMany = [fd202fd102s: Fd202Fd102]

    String  manCreated
    Date    dateCreated = new Date()
    String  manLastUpdated
    Date    lastUpdated = new Date()
    Fd101   fd101
    String  status
    Integer total



    static mapping = {
        table "FD202"
        comment "顧客品項"

        id              column: "OBJID"
        manCreated      column: "MAN_CREATED"       ,comment:"建檔人員"
        dateCreated     column: "DATE_CREATED"      ,comment:"建檔時間"
        manLastUpdated  column: "MAN_LAST_UPDATED"  ,comment:"最後異動人員"
        lastUpdated     column: "LAST_UPDATED"      ,comment:"最後異動時間"
        fd201           column: "FD201_ID"          ,comment:"顧客訂單"
        fd101           column: "FD101_ID"          ,comment:"店家菜單品項"
        status          column: "STATUS"            ,comment:"處理結果"
        total           column: "TOTAL"             ,comment:"數量"

    }


    static constraints = {
        manCreated      (nullable: false)
        dateCreated     (nullable: false)
        manLastUpdated  (nullable:true)
        lastUpdated     (nullable:true)
        fd201           (nullable: false)
        fd101           (nullable:true)
        status          (nullable: false)
        total           (nullable: false)

    }
}