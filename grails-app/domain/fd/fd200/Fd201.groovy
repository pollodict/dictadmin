package fd.fd200

import fd.Fd101Fd102
import fd.fd100.Fd100


/**
 * @author : PDMan v3.5.5
 * @date : 2021-11-22
 * @desc : 顧客訂單
 */
class Fd201{
    static auditable = [ignore: ['dateCreated', 'lastUpdated', 'manCreated', 'manLastUpdated']]
    static belongsTo = [fd200: Fd200]
    static hasMany = [fd202s:Fd202]

    String  manCreated
    Date    dateCreated = new Date()
    String  manLastUpdated
    Date    lastUpdated = new Date()
    String  status
    Date    buyDate
    String  finishDate
    Integer total
    String  remark


    static mapping = {
        table "FD201"
        comment "顧客訂單"

        id              column: "OBJID"
        manCreated      column: "MAN_CREATED"       ,comment:"建檔人員"
        dateCreated     column: "DATE_CREATED"      ,comment:"建檔時間"
        manLastUpdated  column: "MAN_LAST_UPDATED"  ,comment:"最後異動人員"
        lastUpdated     column: "LAST_UPDATED"      ,comment:"最後異動時間"
        fd200           column: "FD200_ID"          ,comment:"顧客資料"
        status          column: "STATUS"            ,comment:"處理狀況"
        buyDate         column: "BUY_DATE"          ,comment:"下訂時間"
        finishDate      column: "FINISH_DATE"       ,comment:"訂單完成時間"
        total           column: "TOTAL"             ,comment:"總計"
        remark          column: "REMARK"            ,comment:"備註"
    }


    static constraints = {
        manCreated      (nullable: false)
        dateCreated     (nullable: false)
        manLastUpdated  (nullable:true)
        fd200           (nullable:true)
        status          (nullable: false)
        buyDate         (nullable: false)
        finishDate      (nullable:true)
        total           (nullable: false)
        remark          (nullable:true)
    }
}
