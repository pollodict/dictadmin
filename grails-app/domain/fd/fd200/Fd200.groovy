package fd.fd200

/**
 * @author : PDMan v3.5.5
 * @date : 2021-11-22
 * @desc : 顧客資料
 */
class Fd200{
    static auditable = [ignore: ['dateCreated', 'lastUpdated', 'manCreated', 'manLastUpdated']]
    static hasMany = [fd201s: Fd201, fd202s: Fd202]

    String  manCreated
    Date    dateCreated = new Date()
    String  manLastUpdated
    Date    lastUpdated = new Date()
    String  name
    String  contactMethod

    static mapping = {
        table "FD200"
        comment "顧客資料"

        id              column: "OBJID"
        manCreated      column: "MAN_CREATED"       ,comment:"建檔人員"
        dateCreated     column: "DATE_CREATED"      ,comment:"建檔時間"
        manLastUpdated  column: "MAN_LAST_UPDATED"  ,comment:"最後異動人員"
        lastUpdated     column: "LAST_UPDATED"      ,comment:"最後異動時間"
        name            column: "NAME"              ,comment:"姓名"
        contactMethod   column: "CONTACT_METHOD"    ,comment:"聯絡方式"

    }


    static constraints = {
        manCreated      (nullable: false)
        dateCreated     (nullable: false)
        manLastUpdated  (nullable:true)
        lastUpdated     (nullable:true)
        name            (nullable: false)
        contactMethod   (nullable:true)
    }
}