package fd

import fd.fd100.Fd102
import fd.fd200.Fd202


/**
 * @author : PDMan v3.5.5
 * @date : 2021-11-22
 * @desc : 顧客品項_附屬品項
 */
class Fd202Fd102{
    static auditable = [ignore: ['dateCreated', 'lastUpdated', 'manCreated', 'manLastUpdated']]
    static belongsTo = [fd202:Fd202]

    String  manCreated
    Date    dateCreated = new Date()
    String  manLastUpdated
    Date    lastUpdated = new Date()
    Fd102   fd102
    Integer total

    static mapping = {
        table "FD202_FD101"
        comment "顧客品項_附屬品項"

        id              column: "OBJID"
        manCreated      column: "MAN_CREATED"       ,comment:"建檔人員"
        dateCreated     column: "DATE_CREATED"      ,comment:"建檔時間"
        manLastUpdated  column: "MAN_LAST_UPDATED"  ,comment:"最後異動人員"
        lastUpdated     column: "LAST_UPDATED"      ,comment:"最後異動時間"
        fd202           column: "FD202_ID"          ,comment:"顧客品項"
        fd102           column: "FD102_ID"          ,comment:"店家菜單品項(附屬品項)"
        total           column: "TOTAL"             ,comment:"數量"

    }

    static constraints = {
        manCreated      (nullable: false)
        dateCreated     (nullable: false)
        manLastUpdated  (nullable:true)
        lastUpdated     (nullable:true)
        fd202           (nullable: false)
        fd102           (nullable: false)
        total           (nullable: false)
    }
}