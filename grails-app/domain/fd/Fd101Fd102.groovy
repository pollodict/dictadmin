package fd

import fd.fd100.Fd101
import fd.fd100.Fd102


/**
 * @author : PDMan v3.5.5
 * @date : 2021-11-22
 * @desc : 品項與附屬品項關係
 */
class Fd101Fd102{
    static auditable = [ignore: ['dateCreated', 'lastUpdated', 'manCreated', 'manLastUpdated']]
    static belongsTo = [fd101: Fd101]

    String  manCreated
    Date    dateCreated = new Date()
    String  manLastUpdated
    Date    lastUpdated = new Date()
    Fd102   fd102

    static mapping = {
        table "FD101_FD102"
        comment "品項與附屬品項關係"

        id              column: "OBJID"
        manCreated      column: "MAN_CREATED"       ,comment:"建檔人員"
        dateCreated     column: "DATE_CREATED"      ,comment:"建檔時間"
        manLastUpdated  column: "MAN_LAST_UPDATED"  ,comment:"最後異動人員"
        lastUpdated     column: "LAST_UPDATED"      ,comment:"最後異動時間"
        fd101           column: "FD101_ID"          ,comment:"品項"
        fd102           column: "FD102_ID"          ,comment:"附屬品項",ignoreNotFound: true

    }


    static constraints = {

        manCreated      (nullable: false)
        dateCreated     (nullable: false)
        manLastUpdated  (nullable:true)
        lastUpdated     (nullable:true)
        fd101           (nullable: false)
        fd102           (nullable: false)
    }
}