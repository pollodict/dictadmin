package fd.fd100

import fd.Fd101Fd102


/**
 * @author : PDMan v3.5.5
 * @date : 2021-11-22
 * @desc : 店家菜單品項
 */
class Fd101{
    static auditable = [ignore: ['dateCreated', 'lastUpdated', 'manCreated', 'manLastUpdated']]
    static belongsTo = [fd100:Fd100]
    static hasMany = [fd101fd102s: Fd101Fd102]


    String  manCreated
    Date    dateCreated = new Date()
    String  manLastUpdated
    Date    lastUpdated = new Date()
    String  name
    Integer atm
    Boolean isLive
    Integer size

    static mapping = {
        table "FD101"
        comment "店家菜單品項"

        id              column: "OBJID"
        manCreated      column: "MAN_CREATED"       ,comment:"建檔人員"
        dateCreated     column: "DATE_CREATED"      ,comment:"建檔時間"
        manLastUpdated  column: "MAN_LAST_UPDATED"  ,comment:"最後異動人員"
        lastUpdated     column: "LAST_UPDATED"      ,comment:"最後異動時間"
        fd100           column: "FD100_ID"          ,comment:"店家資料"
        name            column: "NAME"              ,comment:"品名"
        atm             column: "ATM"               ,comment:"售價"
        isLive          column: "IS_LIVE"           ,comment:"目前有販售"
        size            column: "SIZE"              ,comment:"份量"
    }


    static constraints = {
        manCreated      (nullable: false)
        dateCreated     (nullable: false)
        manLastUpdated  (nullable:true)
        lastUpdated     (nullable:true)
        fd100           (nullable: false)
        name            (nullable: false)
        atm             (nullable: false)
        isLive          (nullable: false)
        size            (nullable:true)
    }
}