package fd.fd100


/**
 * @author : PDMan v3.5.5
 * @date : 2021-11-22
 * @desc : 店家資料-店家資料
 */
class Fd100{
    static auditable = [ignore: ['dateCreated', 'lastUpdated', 'manCreated', 'manLastUpdated']]
    static hasMany = [fd101s:Fd101,fd102s:Fd102]

    String  manCreated
    Date    dateCreated = new Date()
    String  manLastUpdated
    Date    lastUpdated = new Date()
    String  name
    String  addr
    String  tel

    static mapping = {
        table "FD100"
        comment "店家資料-店家資料"

        id              column: "OBJID"
        manCreated      column: "MAN_CREATED"       ,comment:"建檔人員"
        dateCreated     column: "DATE_CREATED"      ,comment:"建檔時間"
        manLastUpdated  column: "MAN_LAST_UPDATED"  ,comment:"最後異動人員"
        lastUpdated     column: "LAST_UPDATED"      ,comment:"最後異動時間"
        name            column: "NAME"              ,comment:"店家名稱"
        addr            column: "ADDR"              ,comment:"地址"
        tel             column: "TEL"               ,comment:"電話"

    }


    static constraints = {
        manCreated      (nullable: false,blank: false,maxSize:32)
        dateCreated     (nullable: false)
        manLastUpdated  (nullable:true,blank:true,maxSize:32)
        lastUpdated     (nullable:true)
        name            (nullable: false)
        addr            (nullable:true)
        tel             (nullable:true)
    }
}