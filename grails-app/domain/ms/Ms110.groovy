package ms


/**
 * @author : PDMan v3.5.5
 * @date : 2022-2-4
 * @desc : 收支資料
 */
class Ms110{
    static auditable = [ignore: ['dateCreated', 'lastUpdated', 'manCreated', 'manLastUpdated']]
    static belongsTo = [ms100I:Ms100]

    String  manCreated
    Date    dateCreated = new Date()
    String  manLastUpdated
    Date    lastUpdated = new Date()
    Integer status1
    Integer status2
    BigDecimal amt
    String  remark

    static mapping = {
        table "MS110"
        comment "收支資料"
        id              column: "OBJID"

        manCreated      column: "MAN_CREATED"       ,comment:"建檔人員"
        dateCreated     column: "DATE_CREATED"      ,comment:"建檔時間"
        manLastUpdated  column: "MAN_LAST_UPDATED"  ,comment:"最後異動人員"
        lastUpdated     column: "LAST_UPDATED"      ,comment:"最後異動時間"
        ms100I         column: "MS100_ID"          ,comment:"帳本"
        status1         column: "STATUS1"           ,comment:"收入或支出"
        status2         column: "STATUS2"           ,comment:"金額類別"
        amt             column: "AMT"               ,comment:"金額"
        remark          column: "REMARK"            ,comment:"備註"
    }

    static constraints = {

        manCreated      (nullable: false)
        dateCreated     (nullable: false)
        manLastUpdated  (nullable:true)
        lastUpdated     (nullable:true)
        status1         (nullable:true)
        status2         (nullable:true)
        amt             (nullable:true)
        remark          (nullable:true)
    }
}
