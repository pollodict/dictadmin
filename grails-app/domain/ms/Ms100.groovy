package ms

/**
 * @author : PDMan v3.5.5
 * @date : 2022-2-4
 * @desc : 帳本
 */
class Ms100{
    static auditable = [ignore: ['dateCreated', 'lastUpdated', 'manCreated', 'manLastUpdated']]
    static hasMany = [ms110L:Ms110]

    String  manCreated
    Date    dateCreated = new Date()
    String  manLastUpdated
    Date    lastUpdated = new Date()
    String  account
    String  accountName

    static mapping = {
        table "MS100"
        comment "帳本"
        id              column: "OBJID"

        manCreated      column: "MAN_CREATED"       ,comment:"建檔人員"
        dateCreated     column: "DATE_CREATED"      ,comment:"建檔時間"
        manLastUpdated  column: "MAN_LAST_UPDATED"  ,comment:"最後異動人員"
        lastUpdated     column: "LAST_UPDATED"      ,comment:"最後異動時間"
        account         column: "ACCOUNT"           ,comment:"帳號"
        accountName     column: "ACCOUNT_NAME"      ,comment:"帳號名稱"
    }

    static constraints = {

        manCreated      (nullable: false)
        dateCreated     (nullable: false)
        manLastUpdated  (nullable:true)
        lastUpdated     (nullable:true)
        account         (nullable: false)
        accountName     (nullable: false)
    }
}