package bs

class Bs100 {

	static auditable = [ignore: ['dateCreated', 'lastUpdated', 'manCreated', 'manLastUpdated']]
	static hasMany = [bs101s:Bs101]

	String		manCreated
	Date		dateCreated = new Date()
	String		manLastUpdated
	Date		lastUpdated
	String		note
	String		systemType
	String		ptype
	String		ptypeDesc


	static mapping = {
		table 'BS100'
		comment 'bs100'
		version true 
		//---1.objid PK1
			id					column:"OBJID"
			manCreated			column:"MAN_CREATED",		comment:"建檔人員"
			dateCreated			column:"DATE_CREATED",		comment:"建檔時間"
			manLastUpdated		column:"MAN_LAST_UPDATED",	comment:"最後異動人員"
			lastUpdated			column:"LAST_UPDATED",		comment:"最後異動時間"
			note				column:"NOTE",				comment:"資料註記"
			systemType			column:"SYSTEM_TYPE",		comment:"系統類別"
			ptype				column:"PTYPE",				comment:"選單類別"
			ptypeDesc			column:"PTYPE_DESC",		comment:"選單說明"
	}


	static constraints = {
		manCreated			(nullable:false, blank: false, maxSize: 20)
		dateCreated			(nullable:false, blank: false)
		manLastUpdated		(nullable:true, maxSize: 50)
		lastUpdated			(nullable:true)
		note				(nullable:true, maxSize: 50)
		systemType			(nullable:false, blank: false, maxSize: 20)
		ptype				(nullable:false, blank: false, maxSize: 20,unique: true ,matches: "[A-Z0-9_]+" )
		ptypeDesc			(nullable:false, blank: false,maxSize: 50)
	}

}

