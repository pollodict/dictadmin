package bs
class BsAppGroup {
	static auditable = [ignore: ['dateCreated', 'lastUpdated', 'manCreated', 'manLastUpdated']]
	String		manCreated
	Date		dateCreated = new Date()
	String		manLastUpdated
	Date		lastUpdated
	String		note
	String		appGroupNo
	String		appGroupName


	static mapping = {
		table 'BS_APP_GROUP'
		comment '程式群組'
		version true
		id					column:"OBJID", generator:"sequence", params: [sequence: "base_sequence"]
		manCreated			column:"MAN_CREATED",		comment:"建檔人員"
		dateCreated			column:"DATE_CREATED",		comment:"建檔時間"
		manLastUpdated		column:"MAN_LAST_UPDATED",	comment:"最後異動人員"
		lastUpdated			column:"LAST_UPDATED",		comment:"最後異動時間"
		note				column:"NOTE",				comment:"資料註記"
		appGroupNo			column:"APP_GROUP_NO",		comment:"程式群組代號"
		appGroupName		column:"APP_GROUP_NAME",	comment:"程式群組名稱"
	}


	static constraints = {
		manCreated			(nullable:false, blank: false, maxSize: 20)
		dateCreated			(nullable:false, blank: false)
		manLastUpdated		(nullable:true, maxSize: 20)
		lastUpdated			(nullable:true)
		note				(nullable:true, maxSize: 50)
		appGroupNo			(nullable:false, blank: false, maxSize: 5,unique: true)
		appGroupName		(nullable:false, blank: false, maxSize: 10)
	}

}

