package bs

class Bs101 {

	static auditable = [ignore: ['dateCreated', 'lastUpdated', 'manCreated', 'manLastUpdated']]
	static belongsTo = [bs100:Bs100]

	String		manCreated
	Date		dateCreated = new Date()
	String		manLastUpdated
	Date		lastUpdated
	String		note
	String		ptype
	Integer		pcode
	String		pdesc
	String		existsString1
	String		existsString2
	Integer		existsNumber1
	Integer		existsNumber2


	static mapping = {
		table 'BS101'
		comment 'bs101'
		version true 
		//---1.objid PK0
			id					column:"OBJID"
			manCreated			column:"MAN_CREATED",		comment:"建檔人員"
			dateCreated			column:"DATE_CREATED",		comment:"建檔時間"
			manLastUpdated		column:"MAN_LAST_UPDATED",	comment:"最後異動人員"
			lastUpdated			column:"LAST_UPDATED",		comment:"最後異動時間"
			note				column:"NOTE",				comment:"資料註記"
			bs100				column:"BS100_ID",			comment:"父層選單",ignoreNotFound: true
			ptype				column:"PTYPE",				comment:"選單類別"
			pcode				column:"PCODE",				comment:"選單代碼"
			pdesc				column:"PDESC",				comment:"選單代碼說明"
			existsString1		column:"EXISTS_STRING1",	comment:"補充文字1"
			existsString2		column:"EXISTS_STRING2",	comment:"補充文字2"
			existsNumber1		column:"EXISTS_NUMBER1",	comment:"補充數字1"
			existsNumber2		column:"EXISTS_NUMBER2",	comment:"補充數字2"
	}


	static constraints = {
		manCreated			(nullable:false, blank: false, maxSize: 20)
		dateCreated			(nullable:false, blank: false)
		manLastUpdated		(nullable:true, maxSize: 50)
		lastUpdated			(nullable:true)
		note				(nullable:true, maxSize: 50)
		ptype				(nullable:false, blank: false, maxSize: 20)
		pcode				(nullable:false, blank: false)
		pdesc				(nullable:false, blank: false, maxSize: 20)
		existsString1		(nullable:true, maxSize: 10)
		existsString2		(nullable:true, maxSize: 10)
		existsNumber1		(nullable:true)
		existsNumber2		(nullable:true)
	}

}

