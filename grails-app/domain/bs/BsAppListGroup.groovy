package bs
class BsAppListGroup {
	static auditable = [ignore: ['dateCreated', 'lastUpdated', 'manCreated', 'manLastUpdated']]
	String		manCreated
	Date		dateCreated = new Date()
	String		manLastUpdated
	Date		lastUpdated
	String		note
	String		appNo
	String		appGroupNo

	String 		appName

	static mapping = {
		table 'BS_APP_LIST_GROUP'
		comment '程式群組清單'
		version true 
		//---1.objid PK0
			id					column:"OBJID", generator:"sequence", params: [sequence: "base_sequence"]
			manCreated			column:"MAN_CREATED",		comment:"建檔人員"
			dateCreated			column:"DATE_CREATED",		comment:"建檔時間"
			manLastUpdated		column:"MAN_LAST_UPDATED",	comment:"最後異動人員"
			lastUpdated			column:"LAST_UPDATED",		comment:"最後異動時間"
			note				column:"NOTE",				comment:"資料註記"
			appNo				column:"app_no",			comment:"程式清單"
			appGroupNo			column:"app_group_no",		comment:"程式群組"
			appName				formula: "(select t.app_name  from bs_request_map t where t.app_no = app_no  ) ", comment: "程式名稱",ignoreNotFound: true
	}


	static constraints = {
		manCreated			(nullable:false, blank: false, maxSize: 20)
		dateCreated			(nullable:false, blank: false)
		manLastUpdated		(nullable:true, maxSize: 20)
		lastUpdated			(nullable:true)
		note				(nullable:true, maxSize: 50)
		appNo				(nullable:false, blank: false , unique:'appGroupNo')
		appGroupNo			(nullable:false, blank: false)
	}

}

