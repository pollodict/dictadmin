package ms

import grails.web.databinding.DataBinder
import grails.gorm.transactions.Transactional
import grails.web.servlet.mvc.GrailsParameterMap
import org.springframework.context.MessageSource


/**
 * @author : PDMan v3.5.5
 * @date : 2022-2-7
 * @desc : 收支資料
 */
@Transactional
class Ms110Service implements DataBinder {

    MessageSource messageSource

    LinkedHashMap filter(GrailsParameterMap params){

        LinkedHashMap result = [:]

        String[] strL = []
        String[] booleanL = []
        String[] likeL = []
        String[] mulSelectL = []
        String[] longL = []

        String[] dtL = []
        def dtSEL = []
        dtL.each {
            dtSEL << "${it}1"
            dtSEL << "${it}2"
        }

        def ms110L = Ms110.createCriteria().list() {

            strL.each {
                if(params?."${it}"){
                    eq(it, params?."${it}")
                }
            }

            longL.each {
                if(params?."${it}"){
                    eq(it, params?.long("${it}"))
                }
            }

            booleanL.each {
                if(params?."${it}"){
                    eq(it, Boolean.parseBoolean(params?."${it}"))
                }
            }

            likeL.each {
                if(params?."${it}"){
                    ilike("${it}", "%"+params?."${it}"+"%")
                }
            }

            dtL.each {
                if(params?."${it}1"){
                    ge("${it}",params?."${it}S")
                }
                if(params?."${it}2"){
                    le("${it}",params?."${it}E")
                }
            }

            mulSelectL.each {
                if(params?."${it}"){
                    def selectL = params?.list("${it}")
                    def columnName = abService.changeStyle(it,false)
                    or {
                        selectL.each { it2 ->
                            sqlRestriction(" instr(',' || '${it2}' || ',' , ',' || this_.${columnName} || ',') > 0  ")
                        }
                    }
                }
            }
            order("id", "desc") // 新資料放在前面, 可依照系統調整
        }

        result.rows = ms110L.collect { it ->
            [
                    ms100Id  :   it?.ms100Id,
                    status1  :   it?.status1,
                    status2  :   it?.status2,
                    amt  :   it?.amt,
                    remark  :   it?.remark,
            ]
        }

        return result
    }


    /**
     * 新增資料
     * @param params
     * @return
     */
    LinkedHashMap insert(GrailsParameterMap params) {
        return _saveInstance(new Ms110(), params, { Ms110 ms110I ->
            ms110I.manCreated = '系統管理員'
            ms110I.validate()
        })
    }


    /**
     * 更新資料
     * @param params
     * @return
     */
    LinkedHashMap update(GrailsParameterMap params){
        return _saveInstance(Ms110.get(params.id as long), params, { Ms110 ms110I ->
            ms110I.manLastUpdated = '系統管理員'
            ms110I.validate()
        })
    }


    /**
     * 共同處理
     * @param ms110I
     * @param params
     * @param closure
     * @return
     */
    LinkedHashMap _saveInstance(Ms110 ms110I, GrailsParameterMap params, Closure<?> closure) {

        LinkedHashMap result = [
                actionType:false,
                acrtionMessage:''
        ]

        result.bean = ms110I
        closure(ms110I)

        List include_col = [
                'ms100Id','status1','status2','amt','remark',
        ]

        bindData(ms110I, params["ms110"], [include:include_col])
        int pageDataVersion = params.ms110.version?(params.ms110?.version as int):0
        if(ms110I.version != pageDataVersion && params.ms110.id){
            result.dataVersionDifferent = true
            ms110I.discard()
        } else if (ms110I.hasErrors()) { //失敗
            def errorColumn = []

            ms110I.errors.allErrors.eachWithIndex  {item, index ->
                errorColumn[index] = [item?.arguments,item?.defaultMessage]
            }

            ms110I.discard()
            result.actionType = false
        }
        else{

            try{
                ms110I.save(flush: true)
                result.actionType = true
            }catch(Exception ex){
                result.actionType = false
                ex.printStackTrace()
                ms110I.discard()
            }

            if(result.actionType){
                result.actionMessage = messageSource.getMessage("default.updated.message", [] as Object[], Locale.TAIWAN)
            }
            else{
                result.actionMessage = messageSource.getMessage("default.updated.message", [] as Object[], Locale.TAIWAN)
            }
        }

        return result
    }


    /**
     * 刪除資料
     * @param params
     * @return
     */
    LinkedHashMap delete(GrailsParameterMap params){
        LinkedHashMap result = [
                actionType : false,
                acrtionMessage : '',
                bean : Ms110
        ]

        Ms110 ms110I = result.bean = Ms110.get(params.id as long)
        int pageDataVersion = params.ms110.version?(params.ms110?.version as int):0
        if(ms110I.version != pageDataVersion && params.ms110.id){
            result.dataVersionDifferent = true
            ms110I.discard()
        }else{

            try{
                ms110I.delete()
                result.actionType = true
            }catch(Exception ex){
                result.actionType = false
                ex.printStackTrace()
                ms110I.discard()
            }

            if(result.actionType){
                result.actionMessage = messageSource.getMessage("default.deleted.message", [] as Object[], Locale.TAIWAN)
            }
            else{
                result.actionMessage = messageSource.getMessage("default.not.deleted.message", [] as Object[], Locale.TAIWAN)
            }
        }

        return result
    }
}