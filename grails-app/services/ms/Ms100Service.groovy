package ms


import grails.web.databinding.DataBinder
import grails.gorm.transactions.Transactional
import grails.web.servlet.mvc.GrailsParameterMap
import org.springframework.context.MessageSource


/**
 * @author : PDMan v3.5.5
 * @date : 2022-2-7
 * @desc : 帳本
 */
@Transactional
class Ms100Service implements DataBinder {

    MessageSource messageSource

    LinkedHashMap filter(GrailsParameterMap params){

        LinkedHashMap result = [:]

        String[] strL = []
        String[] booleanL = []
        String[] likeL = []
        String[] mulSelectL = []
        String[] longL = []

        String[] dtL = []
        def dtSEL = []
        dtL.each {
            dtSEL << "${it}1"
            dtSEL << "${it}2"
        }

        def ms100L = Ms100.createCriteria().list() {

            strL.each {
                if(params?."${it}"){
                    eq(it, params?."${it}")
                }
            }

            longL.each {
                if(params?."${it}"){
                    eq(it, params?.long("${it}"))
                }
            }

            booleanL.each {
                if(params?."${it}"){
                    eq(it, Boolean.parseBoolean(params?."${it}"))
                }
            }

            likeL.each {
                if(params?."${it}"){
                    ilike("${it}", "%"+params?."${it}"+"%")
                }
            }

            dtL.each {
                if(params?."${it}1"){
                    ge("${it}",params?."${it}S")
                }
                if(params?."${it}2"){
                    le("${it}",params?."${it}E")
                }
            }

            mulSelectL.each {
                if(params?."${it}"){
                    def selectL = params?.list("${it}")
                    def columnName = abService.changeStyle(it,false)
                    or {
                        selectL.each { it2 ->
                            sqlRestriction(" instr(',' || '${it2}' || ',' , ',' || this_.${columnName} || ',') > 0  ")
                        }
                    }
                }
            }
            order("id", "desc") // 新資料放在前面, 可依照系統調整
        }

        result.rows = ms100L.collect { it ->
            [
                    account  :   it?.account,
                    accountName  :   it?.accountName,
            ]
        }

        return result
    }


    /**
     * 新增資料
     * @param params
     * @return
     */
    LinkedHashMap insert(GrailsParameterMap params) {
        return _saveInstance(new Ms100(), params, { Ms100 ms100I ->
            ms100I.manCreated = '系統管理員'
            ms100I.validate()
        })
    }


    /**
     * 更新資料
     * @param params
     * @return
     */
    LinkedHashMap update(GrailsParameterMap params){
        return _saveInstance(Ms100.get(params.id as long), params, { Ms100 ms100I ->
            ms100I.manLastUpdated = '系統管理員'
            ms100I.validate()
        })
    }


    /**
     * 共同處理
     * @param ms100I
     * @param params
     * @param closure
     * @return
     */
    LinkedHashMap _saveInstance(Ms100 ms100I, GrailsParameterMap params, Closure<?> closure) {

        LinkedHashMap result = [
                actionType:false,
                acrtionMessage:''
        ]

        result.bean = ms100I
        closure(ms100I)

        List include_col = [
                'account','accountName',
        ]

        bindData(ms100I, params["ms100"], [include:include_col])
        int pageDataVersion = params.ms100.version?(params.ms100?.version as int):0
        if(ms100I.version != pageDataVersion && params.ms100.id){
            result.dataVersionDifferent = true
            ms100I.discard()
        } else if (ms100I.hasErrors()) { //失敗
            def errorColumn = []

            ms100I.errors.allErrors.eachWithIndex  {item, index ->
                errorColumn[index] = [item?.arguments,item?.defaultMessage]
            }

            ms100I.discard()
            result.actionType = false
        }
        else{

            try{
                ms100I.save(flush: true)
                result.actionType = true
            }catch(Exception ex){
                result.actionType = false
                ex.printStackTrace()
                ms100I.discard()
            }

            if(result.actionType){
                result.actionMessage = messageSource.getMessage("default.updated.message", [] as Object[], Locale.TAIWAN)
            }
            else{
                result.actionMessage = messageSource.getMessage("default.updated.message", [] as Object[], Locale.TAIWAN)
            }
        }

        return result
    }


    /**
     * 刪除資料
     * @param params
     * @return
     */
    LinkedHashMap delete(GrailsParameterMap params){
        LinkedHashMap result = [
                actionType : false,
                acrtionMessage : '',
                bean : Ms100
        ]

        Ms100 ms100I = result.bean = Ms100.get(params.id as long)
        int pageDataVersion = params.ms100.version?(params.ms100?.version as int):0
        if(ms100I.version != pageDataVersion && params.ms100.id){
            result.dataVersionDifferent = true
            ms100I.discard()
        }else{

            try{
                ms100I.delete()
                result.actionType = true
            }catch(Exception ex){
                result.actionType = false
                ex.printStackTrace()
                ms100I.discard()
            }

            if(result.actionType){
                result.actionMessage = messageSource.getMessage("default.deleted.message", [] as Object[], Locale.TAIWAN)
            }
            else{
                result.actionMessage = messageSource.getMessage("default.not.deleted.message", [] as Object[], Locale.TAIWAN)
            }
        }

        return result
    }
}