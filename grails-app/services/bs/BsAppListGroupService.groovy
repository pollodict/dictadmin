package bs

import grails.gorm.transactions.Transactional
import grails.web.databinding.DataBinder
import grails.web.servlet.mvc.GrailsParameterMap
import org.springframework.context.MessageSource

@Transactional
class BsAppListGroupService implements DataBinder {
    MessageSource messageSource

    /**
     * 查詢
     * @param params
     * @return
     */
    LinkedHashMap filter(GrailsParameterMap params){

        LinkedHashMap result = [:]

        String[] strL = []
        String[] booleanL = []
        String[] likeL = ['appGroupNo','appGroupName']
        String[] mulSelectL = []
        String[] longL = []

        String[] dtL = []
        ArrayList dtSEL = []
        dtL.each {
            dtSEL << "${it}1"
            dtSEL << "${it}2"
        }

        List<BsAppListGroup> bsAppListGroupL = BsAppListGroup.createCriteria().list(params) {

            strL.each {
                if(params?."${it}"){
                    eq(it, params?."${it}")
                }
            }

            longL.each {
                if(params?."${it}"){
                    eq(it, params?.long("${it}"))
                }
            }

            booleanL.each {
                if(params?."${it}"){
                    eq(it, Boolean.parseBoolean(params?."${it}"))
                }
            }

            likeL.each {
                if(params?."${it}"){
                    ilike("${it}", "%"+params?."${it}"+"%")
                }
            }

            dtL.each {
                if(params?."${it}1"){
                    ge("${it}",params?."${it}S")
                }
                if(params?."${it}2"){
                    le("${it}",params?."${it}E")
                }
            }

            mulSelectL.each {
                if(params?."${it}"){
                    def selectL = params?.list("${it}")
                    def columnName = abService.changeStyle(it,false)
                    or {
                        selectL.each { it2 ->
                            sqlRestriction(" instr(',' || '${it2}' || ',' , ',' || this_.${columnName} || ',') > 0  ")
                        }
                    }
                }
            }

            order("id", "asc")
        }

        result.rows = bsAppListGroupL.collect { it ->
            [

                    id                  : it?.id,
                    appNo               : it?.appNo,
                    appGroupNo          : it?.appGroupNo,
                    appName             : it?.appName,
                    note                : it?.note,
            ]
        }

        return result

    }

    /**
     * 新增資料
     * @param params
     * @return
     */
    LinkedHashMap insert(GrailsParameterMap params) {
        LinkedHashMap result

        result = _saveInstance(new BsAppListGroup(), params, { BsAppListGroup bsAppListGroupI ->
            bsAppListGroupI.manCreated = '系統管理員'
            bsAppListGroupI.validate()
        })

        return result
    }

    /**
     * 儲存
     * @param params
     * @return
     */
    LinkedHashMap update(GrailsParameterMap params){
        LinkedHashMap result

        result =  _saveInstance(BsAppListGroup.get(params.id as long), params, { BsAppListGroup bsAppListGroupI ->
            bsAppListGroupI.manLastUpdated = '系統管理員'
            bsAppListGroupI.validate()
        })
        
        return result
    }

    /**
     * 儲存
     * @param bsAppListGroupI
     * @param params
     * @param closure
     * @return
     */
    LinkedHashMap _saveInstance(
            BsAppListGroup bsAppListGroupI, 
            GrailsParameterMap params, 
            Closure<?> closure
    ){
        LinkedHashMap result = [
                actionType:false,
                acrtionMessage:''        ]

        result.bean = bsAppListGroupI
        closure(bsAppListGroupI)
        List include_col = [
                'appNo','appGroupNo',
        ]

        bindData(bsAppListGroupI, params["bsAppListGroup"], [include:include_col])
        int pageDataVersion = params.bsAppListGroup.version?(params.bsAppListGroup?.version as int):0

        bsAppListGroupI.validate()

        if(bsAppListGroupI.version != pageDataVersion && params.bsAppListGroup.id){
            result.dataVersionDifferent = true
            result.actionType = false
            bsAppListGroupI.discard()
        } else if (bsAppListGroupI.hasErrors()) { //失敗
            def errorColumn = []
            bsAppListGroupI.errors.allErrors.eachWithIndex  {item, index ->
                errorColumn[index] = [item?.arguments,item?.defaultMessage]
            }
            bsAppListGroupI.discard()
            result.actionType = false
        }
        else{
            try{
                bsAppListGroupI.save(flush: true)
                result.actionType = true
            }catch(Exception ex){
                result.actionType = false
                ex.printStackTrace()
                bsAppListGroupI.discard()
            }

            if(result.actionType){
                result.actionMessage = messageSource.getMessage("default.updated.message", [] as Object[], Locale.TAIWAN)
            }
            else{
                result.actionMessage = messageSource.getMessage("default.updated.message", [] as Object[], Locale.TAIWAN)
            }
        }

        return result
    }

    /**
     * 刪除資料
     * @param params
     * @return
     */
    LinkedHashMap delete(GrailsParameterMap params){

        LinkedHashMap result = [
                actionType:false,
                acrtionMessage:''
        ]

        BsAppListGroup bsAppListGroupI = result.bean = BsAppListGroup.get(params.id as long)
        int pageDataVersion = params.bsAppListGroup.version?(params.bsAppListGroup?.version as int):0
        if(bsAppListGroupI.version != pageDataVersion && params.bsAppListGroup.id){

            result.dataVersionDifferent = true
            bsAppListGroupI.discard()
        }else{

            try{
                bsAppListGroupI.delete()
                result.actionType = true
            }catch(Exception ex){
                result.actionType = false
                ex.printStackTrace()
                bsAppListGroupI.discard()
            }

            if(result.actionType){
                result.actionMessage = messageSource.getMessage("default.deleted.message", [] as Object[], Locale.TAIWAN)
            }
            else{
                result.actionMessage = messageSource.getMessage("default.not.deleted.message", [] as Object[], Locale.TAIWAN)
            }
        }

        return result
    }
}
