package bs

import grails.gorm.transactions.Transactional
import grails.web.databinding.DataBinder
import grails.web.servlet.mvc.GrailsParameterMap
import org.springframework.context.MessageSource

/**
 * 系統選單 第一階
 */
@Transactional
class Bs100Service implements DataBinder {

    MessageSource messageSource

    LinkedHashMap filter(GrailsParameterMap params){
        LinkedHashMap result = [:]

        String[] strL = []
        String[] booleanL = []
        String[] likeL = []
        String[] mulSelectL = []
        String[] longL = []

        String[] dtL = []
        def dtSEL = []
        dtL.each {
            dtSEL << "${it}1"
            dtSEL << "${it}2"
        }

        def bs100L = Bs100.createCriteria().list() {

            strL.each {
                if(params?."${it}"){
                    eq(it, params?."${it}")
                }
            }

            longL.each {
                if(params?."${it}"){
                    eq(it, params?.long("${it}"))
                }
            }

            booleanL.each {
                if(params?."${it}"){
                    eq(it, Boolean.parseBoolean(params?."${it}"))
                }
            }

            likeL.each {
                if(params?."${it}"){
                    ilike("${it}", "%"+params?."${it}"+"%")
                }
            }

            dtL.each {
                if(params?."${it}1"){
                    ge("${it}",params?."${it}S")
                }
                if(params?."${it}2"){
                    le("${it}",params?."${it}E")
                }
            }

            mulSelectL.each {
                if(params?."${it}"){
                    def selectL = params?.list("${it}")
                    def columnName = abService.changeStyle(it,false)
                    or {
                        selectL.each { it2 ->
                            sqlRestriction(" instr(',' || '${it2}' || ',' , ',' || this_.${columnName} || ',') > 0  ")
                        }
                    }
                }
            }

            order("id", "desc") // 新資料放在前面, 可依照系統調整
        }

        result.rows = bs100L.collect { it ->
            [

                    id          : it?.id,
                    systemType  : it?.systemType,
                    ptype       : it?.ptype,
                    ptypeDesc   : it?.ptypeDesc,
            ]
        }


        return result

    }

    /**
     * 新增資料
     * @param params
     * @return
     */
    LinkedHashMap insert(GrailsParameterMap params) {
        return _saveInstance(new Bs100(), params, { Bs100 bs100I ->
            bs100I.manCreated = '系統管理員'
            bs100I.validate()
        })
    }

    /**
     * 更新資料
     * @param params
     * @return
     */
    LinkedHashMap update(GrailsParameterMap params){
        return _saveInstance(Bs100.get(params.id as long), params, { Bs100 bs100I ->
            bs100I.manLastUpdated = '系統管理員'
            bs100I.validate()
        })
    }

    /**
     * 共同處理
     * @param bs100I
     * @param params
     * @param closure
     * @return
     */
    LinkedHashMap _saveInstance(Bs100 bs100I, GrailsParameterMap params, Closure<?> closure) {
        LinkedHashMap result = [
                actionType:false,
                acrtionMessage:''
        ]
        result.bean = bs100I
        closure(bs100I)
        List include_col = [
                'systemType','ptype','ptypeDesc',
        ]

        bindData(bs100I, params["bs100"], [include:include_col])
        int pageDataVersion = params.bs100.version?(params.bs100?.version as int):0

        bs100I.validate()

        if(bs100I.version != pageDataVersion && params.bs100.id){
            result.dataVersionDifferent = true
            result.actionType = false
            bs100I.discard()
        } else if (bs100I.hasErrors()) { //失敗
            def errorColumn = []
            bs100I.errors.allErrors.eachWithIndex  {item, index ->
                errorColumn[index] = [item?.arguments,item?.defaultMessage]
            }
            bs100I.discard()
            result.actionType = false
        }
        else{
            try{
                bs100I.save(flush: true)
                result.actionType = true
            }catch(Exception ex){
                result.actionType = false
                ex.printStackTrace()
                bs100I.discard()
            }

            if(result.actionType){
                result.actionMessage = messageSource.getMessage("default.updated.message", [] as Object[], Locale.TAIWAN)
            }
            else{
                result.actionMessage = messageSource.getMessage("default.updated.message", [] as Object[], Locale.TAIWAN)
            }
        }

        return result
    }

    /**
     * 刪除資料
     * @param params
     * @return
     */
    LinkedHashMap delete(GrailsParameterMap params){

        LinkedHashMap result = [
                actionType:false,
                acrtionMessage:''
        ]

        Bs100 bs100I = result.bean = Bs100.get(params.id as long)
        int pageDataVersion = params.bs100.version?(params.bs100?.version as int):0
        if(bs100I.version != pageDataVersion && params.bs100.id){

            result.dataVersionDifferent = true
            bs100I.discard()
        }else{

            try{
                bs100I.delete()
                result.actionType = true
            }catch(Exception ex){
                result.actionType = false
                ex.printStackTrace()
                bs100I.discard()
            }

            if(result.actionType){
                result.actionMessage = messageSource.getMessage("default.deleted.message", [] as Object[], Locale.TAIWAN)
            }
            else{
                result.actionMessage = messageSource.getMessage("default.not.deleted.message", [] as Object[], Locale.TAIWAN)
            }
        }

        return result
    }
}
