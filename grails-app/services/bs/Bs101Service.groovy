package bs

import grails.gorm.transactions.Transactional
import grails.web.databinding.DataBinder
import grails.web.servlet.mvc.GrailsParameterMap
import org.springframework.context.MessageSource

/**
 * 系統選單 第二階
 */
@Transactional
class Bs101Service implements DataBinder {

    MessageSource messageSource

    LinkedHashMap filter(GrailsParameterMap params){

        LinkedHashMap result = [:]

        String[] strL = []
        String[] booleanL = []
        String[] likeL = []
        String[] mulSelectL = []
        String[] longL = []

        String[] dtL = []
        def dtSEL = []
        dtL.each {
            dtSEL << "${it}1"
            dtSEL << "${it}2"
        }

        def bs101L = Bs101.createCriteria().list() {

            strL.each {
                if(params?."${it}"){
                    eq(it, params?."${it}")
                }
            }

            longL.each {
                if(params?."${it}"){
                    eq(it, params?.long("${it}"))
                }
            }

            booleanL.each {
                if(params?."${it}"){
                    eq(it, Boolean.parseBoolean(params?."${it}"))
                }
            }

            likeL.each {
                if(params?."${it}"){
                    ilike("${it}", "%"+params?."${it}"+"%")
                }
            }

            dtL.each {
                if(params?."${it}1"){
                    ge("${it}",params?."${it}S")
                }
                if(params?."${it}2"){
                    le("${it}",params?."${it}E")
                }
            }

            mulSelectL.each {
                if(params?."${it}"){
                    def selectL = params?.list("${it}")
                    def columnName = abService.changeStyle(it,false)
                    or {
                        selectL.each { it2 ->
                            sqlRestriction(" instr(',' || '${it2}' || ',' , ',' || this_.${columnName} || ',') > 0  ")
                        }
                    }
                }
            }

            if(params?.bs100Id){
                long bs100Id = params?.bs100Id as long
                sqlRestriction(" exists(select 1 from bs100 t where t.objid = ? and t.objid = this_.bs100_id )  ",[bs100Id])
            }

            order("id", "desc") // 新資料放在前面, 可依照系統調整
        }

        result.rows = bs101L.collect { it ->
            [

                    id          : it?.id,
                    pcode       : it?.pcode,
                    pdesc       : it?.pdesc,
                    existsString1       : it?.existsString1,
                    existsString2       : it?.existsString2,
                    existsNumber1       : it?.existsNumber1,
                    existsNumber2       : it?.existsNumber2,
            ]
        }


        return result

    }

    /**
     * 新增資料
     * @param params
     * @return
     */
    LinkedHashMap insert(GrailsParameterMap params) {
        return _saveInstance(new Bs101(), params, { Bs101 bs101I ->
            bs101I.bs100 = Bs100.get(params?.bs101.bs100 as long)
            bs101I.manCreated = '系統管理員'
            bs101I.validate()
        })
    }

    /**
     * 更新資料
     * @param params
     * @return
     */
    LinkedHashMap update(GrailsParameterMap params){
        return _saveInstance(Bs101.get(params.id as long), params, { Bs101 bs101I ->
            bs101I.manLastUpdated = '系統管理員'
            bs101I.validate()
        })
    }

    /**
     * 共同處理
     * @param bs101I
     * @param params
     * @param closure
     * @return
     */
    LinkedHashMap _saveInstance(Bs101 bs101I, GrailsParameterMap params, Closure<?> closure) {
        LinkedHashMap result = [
                actionType:false,
                acrtionMessage:''
        ]
        result.bean = bs101I
        closure(bs101I)

        List include_col = [
                'ptype','pcode','pdesc',
                'existsString1',',existsString2',
                'existsNumber1','existsNumber2'
        ]
        bindData(bs101I, params["bs101"], [include:include_col])
        int pageDataVersion = params.bs101.version?(params.bs101?.version as int):0
        if(bs101I.version != pageDataVersion && params.bs101.id){

            result.dataVersionDifferent = true
            bs101I.discard()
        } else if (bs101I.hasErrors()) { //失敗

            def errorColumn = []
            bs101I.errors.allErrors.eachWithIndex  {item, index ->
                errorColumn[index] = [item?.arguments,item?.defaultMessage]
            }
            bs101I.discard()
            result.actionType = false
        }
        else{
            try{
                bs101I.save(flush: true)
                result.actionType = true
            }catch(Exception ex){
                result.actionType = false
                ex.printStackTrace()
                bs101I.discard()
            }

            if(result.actionType){
                result.actionMessage = messageSource.getMessage("default.updated.message", [] as Object[], Locale.TAIWAN)
            }
            else{
                result.actionMessage = messageSource.getMessage("default.updated.message", [] as Object[], Locale.TAIWAN)
            }
        }

        return result
    }

    /**
     * 刪除資料
     * @param params
     * @return
     */
    LinkedHashMap delete(GrailsParameterMap params){

        LinkedHashMap result = [
                actionType:false,
                acrtionMessage:''
        ]

        Bs101 bs101I = result.bean = Bs101.get(params.id as long)
        int pageDataVersion = params.bs101.version?(params.bs101?.version as int):0
        if(bs101I.version != pageDataVersion && params.bs101.id){

            result.dataVersionDifferent = true
            bs101I.discard()
        }else{

            try{
                bs101I.delete()
                result.actionType = true
            }catch(Exception ex){
                result.actionType = false
                ex.printStackTrace()
                bs101I.discard()
            }

            if(result.actionType){
                result.actionMessage = messageSource.getMessage("default.deleted.message", [] as Object[], Locale.TAIWAN)
            }
            else{
                result.actionMessage = messageSource.getMessage("default.not.deleted.message", [] as Object[], Locale.TAIWAN)
            }
        }

        return result
    }
}
