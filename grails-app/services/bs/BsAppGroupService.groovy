package bs

import grails.gorm.transactions.Transactional
import grails.web.databinding.DataBinder
import grails.web.servlet.mvc.GrailsParameterMap
import org.springframework.context.MessageSource

@Transactional
class BsAppGroupService implements DataBinder {

    MessageSource messageSource

    /**
     * 查詢
     * @param params
     * @return
     */
    LinkedHashMap filter(GrailsParameterMap params){

        LinkedHashMap result = [:]

        String[] strL = []
        String[] booleanL = []
        String[] likeL = ['appGroupNo','appGroupName']
        String[] mulSelectL = []
        String[] longL = []

        String[] dtL = []
        ArrayList dtSEL = []
        dtL.each {
            dtSEL << "${it}1"
            dtSEL << "${it}2"
        }

        def bsAppGroupL = BsAppGroup.createCriteria().list(params) {

            strL.each {
                if(params?."${it}"){
                    eq(it, params?."${it}")
                }
            }

            longL.each {
                if(params?."${it}"){
                    eq(it, params?.long("${it}"))
                }
            }

            booleanL.each {
                if(params?."${it}"){
                    eq(it, Boolean.parseBoolean(params?."${it}"))
                }
            }

            likeL.each {
                if(params?."${it}"){
                    ilike("${it}", "%"+params?."${it}"+"%")
                }
            }

            dtL.each {
                if(params?."${it}1"){
                    ge("${it}",params?."${it}S")
                }
                if(params?."${it}2"){
                    le("${it}",params?."${it}E")
                }
            }

            mulSelectL.each {
                if(params?."${it}"){
                    def selectL = params?.list("${it}")
                    def columnName = abService.changeStyle(it,false)
                    or {
                        selectL.each { it2 ->
                            sqlRestriction(" instr(',' || '${it2}' || ',' , ',' || this_.${columnName} || ',') > 0  ")
                        }
                    }
                }
            }

            order("id", "asc")
        }

        result.rows = bsAppGroupL.collect { it ->
            [

                    id                  : it?.id,
                    note                : it?.note,
                    appGroupNo          : it?.appGroupNo,
                    appGroupName        : it?.appGroupName,
            ]
        }

        return result

    }

    /**
     * 新增資料
     * @param params
     * @return
     */
    LinkedHashMap insert(GrailsParameterMap params) {
        return _saveInstance(new BsAppGroup(), params, { BsAppGroup bsAppGroupI ->
            bsAppGroupI.manCreated = '系統管理員'
            bsAppGroupI.validate()
        })
    }

    /**
     * 更新資料
     * @param params
     * @return
     */
    LinkedHashMap update(GrailsParameterMap params){
        return _saveInstance(BsAppGroup.get(params.id as long), params, { BsAppGroup bsAppGroupI ->
            bsAppGroupI.manLastUpdated = '系統管理員'
            bsAppGroupI.validate()
        })
    }

    /**
     * 共同處理
     * @param bsAppGroupI
     * @param params
     * @param closure
     * @return
     */
    LinkedHashMap _saveInstance(BsAppGroup bsAppGroupI, GrailsParameterMap params, Closure<?> closure) {
        LinkedHashMap result = [
                actionType:false,
                acrtionMessage:''
        ]
        result.bean = bsAppGroupI
        closure(bsAppGroupI)
        List include_col = [
                'note','appGroupNo','appGroupName',
        ]

        bindData(bsAppGroupI, params["bsAppGroup"], [include:include_col])
        int pageDataVersion = params.bsAppGroup.version?(params.bsAppGroup?.version as int):0

        bsAppGroupI.validate()

        if(bsAppGroupI.version != pageDataVersion && params.bsAppGroup.id){
            result.dataVersionDifferent = true
            result.actionType = false
            bsAppGroupI.discard()
        } else if (bsAppGroupI.hasErrors()) { //失敗
            def errorColumn = []
            bsAppGroupI.errors.allErrors.eachWithIndex  {item, index ->
                errorColumn[index] = [item?.arguments,item?.defaultMessage]
            }
            bsAppGroupI.discard()
            result.actionType = false
        }
        else{
            try{
                bsAppGroupI.save(flush: true)
                result.actionType = true
            }catch(Exception ex){
                result.actionType = false
                ex.printStackTrace()
                bsAppGroupI.discard()
            }

            if(result.actionType){
                result.actionMessage = messageSource.getMessage("default.updated.message", [] as Object[], Locale.TAIWAN)
            }
            else{
                result.actionMessage = messageSource.getMessage("default.updated.message", [] as Object[], Locale.TAIWAN)
            }
        }

        return result
    }

    /**
     * 刪除資料
     * @param params
     * @return
     */
    LinkedHashMap delete(GrailsParameterMap params){

        LinkedHashMap result = [
                actionType:false,
                acrtionMessage:''
        ]

        BsAppGroup bsAppGroupI = result.bean = BsAppGroup.get(params.id as long)
        int pageDataVersion = params.bsAppGroup.version?(params.bsAppGroup?.version as int):0
        if(bsAppGroupI.version != pageDataVersion && params.bsAppGroup.id){

            result.dataVersionDifferent = true
            bsAppGroupI.discard()
        }else{

            try{
                bsAppGroupI.delete()
                result.actionType = true
            }catch(Exception ex){
                result.actionType = false
                ex.printStackTrace()
                bsAppGroupI.discard()
            }

            if(result.actionType){
                result.actionMessage = messageSource.getMessage("default.deleted.message", [] as Object[], Locale.TAIWAN)
            }
            else{
                result.actionMessage = messageSource.getMessage("default.not.deleted.message", [] as Object[], Locale.TAIWAN)
            }
        }

        return result
    }
}
