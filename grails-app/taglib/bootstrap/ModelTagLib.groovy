package bootstrap

/**
 * model 類別
 * doc: https://getbootstrap.com/docs/5.0/components/modal/#fullscreen-modal
 */
class ModelTagLib {
    static defaultEncodeAs = [taglib:'none']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]

    static namespace = "bootstrap"

    /**
     * model 彈跳視窗
     */
    Closure modal = {attrs,body ->

        if (!attrs.modelId) {
            throwTagError("Tag [modal] is missing required attribute [modelId]")
        }

        String modelId = attrs.remove("modelId")

        out <<
                """
            <div class="modal fade" id="${modelId}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-hidden="true">
                <div class="modal-dialog modal-fullscreen">
                    <div class="modal-content">
                    </div>
                </div>
            </div>
            """
    }

    Closure modelHeader = {attrs,body ->
        String modelTitle = attrs?.remove("modelTitle")?:""
        out <<
                """
            <div class="modal-header">
                <h5 class="modal-title">${modelTitle}</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            """
    }

    Closure modelBody = { attrs, body ->
        String modelAlertId = attrs.remove("model-alert")?:"model-alert"
        String formName = attrs.remove("formName")
        out << """<div class="modal-body">"""
        out << """<span id="${modelAlertId}"></span>"""
        if(formName){
            out << g.form(name:formName,body())
        }else{
            out << body()
        }

        out << """</div>"""
    }

    Closure modelFooter = { attrs, body ->
        out << """<div class="modal-footer">"""
        out << """<button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>"""
        out << body()
        out << """</div>"""
    }
}
