package bootstrap.form

import bootstrap.tag.Attribute
import groovy.transform.CompileStatic
import org.grails.buffer.GrailsPrintWriter
import org.grails.encoder.CodecLookup
import org.grails.encoder.Encoder

/**
 * 系統表單：下拉式選單
 * doc: https://getbootstrap.com/docs/5.0/forms/select/
 */
class SelectTagLib {
    static defaultEncodeAs = [taglib:'none']
    //static encodeAsForTags = [tagName: [taglib:'html'], otherTagName: [taglib:'none']]
    static namespace = "bootstrap"

    CodecLookup codecLookup

    /**
     * 下拉式選單
     */
    Closure select = { attrs ->

        if (!attrs.name) {
            throwTagError("Tag [select] is missing required attribute [name]")
        }
        if (!attrs.containsKey('from')) {
            throwTagError("Tag [select] is missing required attribute [from]")
        }

        GrailsPrintWriter writer = out

        String id = attrs.remove('id')?:attrs.name
        String name = attrs.remove('name')
        String value = attrs.remove('value').toString()


        def selectClass = attrs.remove('class')
        ArrayList from = attrs.remove('from')
        boolean selectDisabled = attrs.remove('selectDisabled')?:false
        String onclick = attrs.remove("onchange")
        String onchange = attrs.remove("onchange")
        String nextSelectId = attrs.remove("nextSelectId")
        String nextSelectUrl = attrs.remove("nextSelectUrl")

        String optionKey = attrs.remove('optionKey')
        String optionValue = attrs.remove('optionValue')

        String placeholder = attrs.remove('placeholder')

        def noSelection = attrs.remove('noSelection')
        if (noSelection != null) {
            noSelection = noSelection.entrySet().iterator().next()
        }

        Attribute attribute = new Attribute()

        attrs = attribute.booleanToAttribute(attrs, 'disabled')
        attrs = attribute.booleanToAttribute(attrs, 'readonly')

        writer << "<select "

        writer << "id='${id}' "
        writer << "name='${name}' "

        writer << "class=' form-control w-auto d-inline "
        if(selectClass){
            writer << selectClass
        }
        writer << " '"

        outputAttributes(attrs, writer, true)


        if(selectDisabled){
            writer << " disabled "
        }

        if(onclick){
            writer << " onclick='${onclick}' "
        }

        if(onchange){
            writer << " onchange='${onchange}' "
        }

        /**
         * 下階層選單
         */
        if(nextSelectUrl && nextSelectId){
            writer << """onchange='ajaxChangSelectOption(this.value,"${nextSelectId}","${nextSelectUrl}");'"""
        }

        if(placeholder){
            writer << " placeholder='${placeholder}' "
        }

        writer << '>'
        writer.println()

        if (noSelection) {
            writer << renderNoSelectionOptionImpl(noSelection.key, noSelection.value, value)
        }

        writer << optionList(from,value,optionKey,optionValue)
        // close tag
        writer << '</select>'
    }

    /**
     * 未選擇選項
     * @param out
     * @param noSelectionKey
     * @param noSelectionValue
     * @param value
     * @return
     */
    private String renderNoSelectionOptionImpl(noSelectionKey, noSelectionValue, value) {

        String returnVal
        // If a label for the '--Please choose--' first item is supplied, write it out
        returnVal =  "<option value=\"${(noSelectionKey == null ? '' : noSelectionKey)}\"${noSelectionKey == value ? ' selected="selected"' : ''}>${noSelectionValue.encodeAsHTML()}</option>"

        return returnVal
    }

    /**
     *
     * @param from
     * @param value
     * @param optionKey
     * @param optionValue
     * @return option清單
     */
    private String optionList(ArrayList from,String value,String optionKey,String optionValue){
        StringBuffer returnValue = new StringBuffer()

        // create options from list
        from.eachWithIndex {el, i ->
            def keyValue = el[optionKey]
            def text = el[optionValue]
            boolean selected
            returnValue.append('<option ')
            returnValue.append(" value='${keyValue}'' ")

            selected = value.equals(keyValue.toString())

            if(selected){
                returnValue.append("selected")
            }
            returnValue.append(' >')
            if(!keyValue.equals('')){
                returnValue.append("${i+1}.${text}")
            }
            else{
                returnValue.append("${text}")
            }

            returnValue.append('</option>')
        }

        return returnValue.toString()
    }

    /**
     * Dump out attributes in HTML compliant fashion.
     */
    @CompileStatic
    private void outputAttributes(Map attrs, GrailsPrintWriter writer, boolean useNameAsIdIfIdDoesNotExist = false) {
        attrs.remove('tagName') // Just in case one is left
        Encoder htmlEncoder = codecLookup?.lookupEncoder('HTML')
        attrs.each { k, v ->
            if (v != null) {
                writer << k
                writer << '="'
                writer << (htmlEncoder != null ? htmlEncoder.encode(v) : v)
                writer << '" '
            }
        }
        if (useNameAsIdIfIdDoesNotExist) {
            outputNameAsIdIfIdDoesNotExist(attrs, writer)
        }
    }

    @CompileStatic
    private void outputNameAsIdIfIdDoesNotExist(Map attrs, GrailsPrintWriter out) {
        if (!attrs.containsKey('id') && attrs.containsKey('name')) {
            Encoder htmlEncoder = codecLookup?.lookupEncoder('HTML')
            out << 'id="'
            out << (htmlEncoder != null ? htmlEncoder.encode(attrs.name) : attrs.name)
            out << '" '
        }
    }
}
